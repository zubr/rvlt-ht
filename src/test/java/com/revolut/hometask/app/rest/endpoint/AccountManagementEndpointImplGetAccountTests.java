package com.revolut.hometask.app.rest.endpoint;

import com.revolut.hometask.app.business.Account;
import com.revolut.hometask.app.business.AccountManagementService;
import com.revolut.hometask.app.business.ExchangeRateService;
import com.revolut.hometask.app.business.MoneyTransferService;
import com.revolut.hometask.app.errors.AccountNotFoundException;
import com.revolut.hometask.app.errors.ApplicationDataAccessError;
import com.revolut.hometask.app.rest.AccountDescription;
import com.revolut.hometask.app.rest.impl.AccountManagementEndpointImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class AccountManagementEndpointImplGetAccountTests {

    private final String accountId = "junit-account";

    private final String currencyCodeEUR = "EUR";

    @InjectMocks
    private AccountManagementEndpointImpl service;

    @Mock
    private AccountManagementService accountManagementService;

    @Mock
    private ExchangeRateService exchangeRateService;

    @Mock
    private MoneyTransferService moneyTransferService;

    @Test
    public void getAllIsOk() throws AccountNotFoundException, ApplicationDataAccessError {
        final Account t = new Account();
        t.setAccountId(accountId);
        t.setBalance(new BigDecimal("123.45"));
        t.setCurrency(currencyCodeEUR);
        when(accountManagementService.get(eq(accountId))).thenReturn(t);

        final AccountDescription accountDescription = service.get(accountId);

        Assert.assertEquals(accountId, accountDescription.getAccountId());
        Assert.assertEquals("123.45", accountDescription.getBalance());
        Assert.assertEquals(currencyCodeEUR, accountDescription.getCurrency());
    }

    @Test(expected = AccountNotFoundException.class)
    public void get_account_does_not_exist() throws AccountNotFoundException, ApplicationDataAccessError {
        when(accountManagementService.get(anyString())).thenThrow(new AccountNotFoundException("not-found"));

        service.get(accountId);
    }

}
