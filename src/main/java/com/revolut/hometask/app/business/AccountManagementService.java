package com.revolut.hometask.app.business;

import com.revolut.hometask.app.errors.*;

import java.math.BigDecimal;

public interface AccountManagementService {

    /**
     * Creates an account with empty balance.
     * @param accountId account id
     * @param currency account currency
     * @throws AccountAlreadyExistsException thrown if account already exists
     */
    void create(String accountId, String currency) throws AccountAlreadyExistsException, ApplicationDataAccessError;

    /**
     * Updates forcefully account money amount
     * @param accountId account id
     * @param accountVersion last seen timestamp for operation and concurrency
     * @param amount amount of money in account's currency
     * @throws AccountConcurrentModificationException thrown if version is outdated
     */
    void update(String accountId, long accountVersion, BigDecimal amount) throws ApplicationDataAccessError, AccountConcurrentModificationException;

    /**
     * Returns account details
     * @param accountId account id
     * @return account object
     * @throws AccountNotFoundException thrown if account does not exist
     */
    Account get(String accountId) throws AccountNotFoundException, ApplicationDataAccessError;

}
