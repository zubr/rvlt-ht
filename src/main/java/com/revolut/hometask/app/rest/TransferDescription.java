package com.revolut.hometask.app.rest;

public class TransferDescription {

    private String accountSourceId;

    private String accountDestinationId;

    private String amount;

    private String currency;

    private long when;

    public String getAccountSourceId() {
        return accountSourceId;
    }

    public void setAccountSourceId(String accountSourceId) {
        this.accountSourceId = accountSourceId;
    }

    public String getAccountDestinationId() {
        return accountDestinationId;
    }

    public void setAccountDestinationId(String accountDestinationId) {
        this.accountDestinationId = accountDestinationId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public long getWhen() {
        return when;
    }

    public void setWhen(long when) {
        this.when = when;
    }
}
