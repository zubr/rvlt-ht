package com.revolut.hometask.app.errors;

public class AccountNotFoundException extends ApplicationException {

    private final String accountId;

    public AccountNotFoundException(String accountId) {
        super("Account not found. AccountId=" + accountId);
        this.accountId = accountId;
    }

    public String getAccountId() {
        return accountId;
    }
}
