package com.revolut.hometask.app.rest.endpoint;

import com.revolut.hometask.app.business.AccountManagementService;
import com.revolut.hometask.app.business.ExchangeRateService;
import com.revolut.hometask.app.business.MoneyTransferService;
import com.revolut.hometask.app.errors.AccountAlreadyExistsException;
import com.revolut.hometask.app.errors.ApplicationDataAccessError;
import com.revolut.hometask.app.rest.impl.AccountManagementEndpointImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class AccountManagementEndpointImplCreateAccountTests {

    private final String ACCOUNT_NAME_FOR_TESTS = "junit-account";

    private final String currencyCodeEUR = "EUR";

    private final String currencyCodeInvalid = "Invalid";

    @InjectMocks
    private AccountManagementEndpointImpl service;

    @Mock
    private AccountManagementService accountManagementService;

    @Mock
    private ExchangeRateService exchangeRateService;

    @Mock
    private MoneyTransferService moneyTransferService;

    @Test
    public void createAllIsOk() throws AccountAlreadyExistsException, ApplicationDataAccessError {
        when(exchangeRateService.isCurrencyValid(eq(currencyCodeEUR))).thenReturn(Boolean.TRUE);

        service.create(ACCOUNT_NAME_FOR_TESTS, currencyCodeEUR);

        verify(exchangeRateService, Mockito.times(1)).isCurrencyValid(eq(currencyCodeEUR));
        verify(accountManagementService, Mockito.times(1)).create(eq(ACCOUNT_NAME_FOR_TESTS), eq(currencyCodeEUR));
    }

    @Test(expected = AccountAlreadyExistsException.class)
    public void create_duplicate_account() throws AccountAlreadyExistsException, ApplicationDataAccessError {
        when(exchangeRateService.isCurrencyValid(eq(currencyCodeEUR))).thenReturn(Boolean.TRUE);
        doThrow(new AccountAlreadyExistsException(ACCOUNT_NAME_FOR_TESTS))
                .when(accountManagementService).create(anyString(), anyString());

        service.create(ACCOUNT_NAME_FOR_TESTS, currencyCodeEUR);
    }

}
