package com.revolut.hometask.app.rest.impl;

import java.math.BigDecimal;

public class ValidationUtils {
    public static BigDecimal getAndValidateAmount(String amountAsString) {
        BigDecimal amount = null;
        if (amountAsString == null || amountAsString.trim().length() == 0) {
            throw new IllegalArgumentException("Transfer amount is missing.");
        }
        try {
            amount = new BigDecimal(amountAsString);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Transfer amount is wrong " + amountAsString);
        }
        if (amount.compareTo(BigDecimal.ZERO) <= 0) {
            throw new IllegalArgumentException("Transfer amount should be bigger than zero.");
        }
        return amount;
    }

    public static void isAccountIdValid(String accountId) {
        if (accountId == null || accountId.trim().length() != accountId.length()
                || accountId.length() < 8 || accountId.length() > 63) {
            throw new IllegalArgumentException("AccountId is invalid " + accountId);
        }
    }
}
