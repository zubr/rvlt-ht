package com.revolut.hometask.app.rest;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.mockito.Mockito;
import spark.Spark;

public class AccountRESTEndpointTestsBase {

    protected static RestEndpointConfig restConfig;

    protected static AccountManagementEndpoint accountManagementEndpoint;

    protected static TransferManagementEndpoint transferManagementEndpoint;

    protected static TransferQueryEndpoint transferQueryEndpoint;

    @BeforeClass
    public static void setup() {
        accountManagementEndpoint = Mockito.mock(AccountManagementEndpoint.class);
        transferManagementEndpoint = Mockito.mock(TransferManagementEndpoint.class);
        transferQueryEndpoint = Mockito.mock(TransferQueryEndpoint.class);

        restConfig = new RestEndpointConfig(accountManagementEndpoint,
                transferManagementEndpoint,
                transferQueryEndpoint);

        restConfig.run(9090);
        Spark.awaitInitialization();
    }

    @AfterClass
    public static void cleanup() {
        Spark.awaitStop();
        Spark.stop();
    }

    @After
    public void reset() {
        Mockito.reset(accountManagementEndpoint);
    }
}
