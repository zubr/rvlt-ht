---
- config:
    - testset: "Basic Account Creation and Management tests"
    - timeout: 1

- test:
    - name: "Create Account"
    - url: "/rest/v1/account"
    - method: "POST"
    - body: {template: '{"accountId": "ACCOUNT_REF_01", "currency": "EUR"}' }
    - headers: {'Content-Type': 'application/json'}
    - expected_status: [201, 409]

- test:
    - name: "Create Account"
    - url: "/rest/v1/account"
    - method: "POST"
    - body: {template: '{"accountId": "ACCOUNT_REF_02", "currency": "EUR"}' }
    - headers: {'Content-Type': 'application/json'}
    - expected_status: [201, 409]

- test:
    - variable_binds: {account_id: "$main_account_id_"}
    - name: "Try to create account that already exists"
    - url: "/rest/v1/account"
    - method: "POST"
    - body: {template: '{"accountId": "ACCOUNT_REF_01", "currency": "EUR"}' }
    - headers: {'Content-Type': 'application/json'}
    - expected_status: [409]
    - validators:
          - extract_test: {jsonpath_mini: "description",  test: "exists"}

- test:
    - name: "Get account details"
    - url: {template: "/rest/v1/account/ACCOUNT_REF_01"}
    - method: "GET"
    - expected_status: [200]
    - extract_binds:
          - 'account_version': {'jmespath': 'version'}
    - validators:
          - compare: {header: 'content-type', expected: 'application/json'}
          - compare: {jsonpath_mini: "currency",  comparator: "eq", expected: "EUR"}
          - extract_test: {jsonpath_mini: "balance",  test: "exists"}
          - extract_test: {jsonpath_mini: "version",  test: "exists"}

- test:
    - name: "Force set account amount. All valid."
    - url: {template: "/rest/v1/account/ACCOUNT_REF_01"}
    - method: "PUT"
    - body: {template: '{"amount": "100.0", "version": "$account_version"}' }
    - expected_status: [200]

- test:
    - name: "Force set account amount. Invalid version."
    - url: {template: "/rest/v1/account/ACCOUNT_REF_01"}
    - method: "PUT"
    - body: {template: '{"amount": "55.0", "version": "-1"}' }
    - expected_status: [409]

- test:
    - name: "Invalid version string"
    - url: {template: "/rest/v1/account/ACCOUNT_REF_01"}
    - method: "PUT"
    - body: {template: '{"amount": "55.0", "version": "invalid"}' }
    - expected_status: [400]

- test:
    - name: "Missing version string"
    - url: {template: "/rest/v1/account/ACCOUNT_REF_01"}
    - method: "PUT"
    - body: {template: '{"amount": "55.0"}' }
    - expected_status: [400]

- test:
    - name: "Invalid amount string"
    - url: {template: "/rest/v1/account/ACCOUNT_REF_01"}
    - method: "PUT"
    - body: {template: '{"amount": "AA.0", "version": "-1"}' }
    - expected_status: [400]

- test:
    - name: "Missing amount string"
    - url: {template: "/rest/v1/account/ACCOUNT_REF_01"}
    - method: "PUT"
    - body: {template: '{"version": "-1"}' }
    - expected_status: [400]

- test:
    - name: "Malformed"
    - url: {template: "/rest/v1/account/ACCOUNT_REF_01"}
    - method: "PUT"
    - body: 'malformed'
    - expected_status: [418]

- test:
    - name: "Malformed2"
    - url: {template: "/rest/v1/account/ACCOUNT_REF_01"}
    - method: "PUT"
    - body: '{ malformed: }'
    - expected_status: [418]

- test:
      - name: "Verify account balance after force-update. Account01."
      - url: {template: "/rest/v1/account/ACCOUNT_REF_01"}
      - method: "GET"
      - expected_status: [200]
      - extract_binds:
            - 'account_version': {'jsonpath_mini': 'version'}
      - validators:
            - compare: {header: 'content-type', expected: 'application/json'}
            - compare: {jsonpath_mini: "currency",  comparator: "eq", expected: "EUR"}
            - compare: {jsonpath_mini: "balance",  comparator: "eq", expected: "100.0"}
            - extract_test: {jsonpath_mini: "version",  test: "exists"}

- test:
      - name: "Transfer money. Account01 -> Account02"
      - url: {template: "/rest/v1/account/ACCOUNT_REF_01/transfer"}
      - method: "POST"
      - expected_status: [201]
      - body: {template: '{"destinationAccount": "ACCOUNT_REF_02", "currency": "EUR", "amount": "10.0", "id": "MY"}' }

- test:
    - name: "Verify account balance after transfer. Account01."
    - url: {template: "/rest/v1/account/ACCOUNT_REF_01"}
    - method: "GET"
    - expected_status: [200]
    - extract_binds:
        - 'account_version': {'jsonpath_mini': 'version'}
    - validators:
        - compare: {header: 'content-type', expected: 'application/json'}
        - compare: {jsonpath_mini: "currency",  comparator: "eq", expected: "EUR"}
        - compare: {jsonpath_mini: "balance",  comparator: "eq", expected: "90.0"}
        - extract_test: {jsonpath_mini: "version",  test: "exists"}

- test:
      - name: "Transfer money, retry to make a transfer attempt. Account01 -> Account02, going to fail"
      - url: {template: "/rest/v1/account/ACCOUNT_REF_01/transfer"}
      - method: "POST"
      - expected_status: [409]
      - body: {template: '{"destinationAccount": "ACCOUNT_REF_02", "currency": "EUR", "amount": "10.0", "id": "MY"}' }

- test:
    - name: "Verify account balance after transfer. Account01."
    - url: {template: "/rest/v1/account/ACCOUNT_REF_01"}
    - method: "GET"
    - expected_status: [200]
    - extract_binds:
        - 'account_version': {'jsonpath_mini': 'version'}
    - validators:
        - compare: {header: 'content-type', expected: 'application/json'}
        - compare: {jsonpath_mini: "currency",  comparator: "eq", expected: "EUR"}
        - compare: {jsonpath_mini: "balance",  comparator: "eq", expected: "90.0"}
        - extract_test: {jsonpath_mini: "version",  test: "exists"}

