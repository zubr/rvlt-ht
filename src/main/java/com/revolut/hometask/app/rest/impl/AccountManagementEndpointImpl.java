package com.revolut.hometask.app.rest.impl;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.revolut.hometask.app.business.Account;
import com.revolut.hometask.app.business.AccountManagementService;
import com.revolut.hometask.app.business.ExchangeRateService;
import com.revolut.hometask.app.errors.AccountAlreadyExistsException;
import com.revolut.hometask.app.errors.AccountConcurrentModificationException;
import com.revolut.hometask.app.errors.AccountNotFoundException;
import com.revolut.hometask.app.errors.ApplicationDataAccessError;
import com.revolut.hometask.app.rest.AccountDescription;
import com.revolut.hometask.app.rest.AccountManagementEndpoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

@Singleton
public class AccountManagementEndpointImpl implements AccountManagementEndpoint {

    private final static Logger LOG = LoggerFactory.getLogger(AccountManagementEndpointImpl.class.getCanonicalName());

    private final AccountManagementService accountManagementService;

    private final ExchangeRateService exchangeRateService;

    @Inject
    public AccountManagementEndpointImpl(AccountManagementService accountManagementService,
                                         ExchangeRateService exchangeRateService) {
        this.accountManagementService = accountManagementService;
        this.exchangeRateService = exchangeRateService;
    }

    @Override
    public void create(String accountId, String currency) throws AccountAlreadyExistsException, ApplicationDataAccessError {
        /// validate input parameters
        isCurrencyCodeValid(currency);
        ValidationUtils.isAccountIdValid(accountId);

        /// execute logic of account generation
        LOG.info("Creating account {} with currency code {}", accountId, currency);
        accountManagementService.create(accountId, currency);
        LOG.info("Created account {} with currency code {}", accountId, currency);
    }

    @Override
    public void update(String accountId, Long accountVersion, String amountAsString) throws ApplicationDataAccessError,
            AccountConcurrentModificationException, AccountNotFoundException {
        /// validate input parameters
        ValidationUtils.isAccountIdValid(accountId);
        final BigDecimal amount = ValidationUtils.getAndValidateAmount(amountAsString);
        if (accountVersion == null) {
            throw new IllegalArgumentException("Version is missing.");
        }

        /// ensure account exists
        final Account account = accountManagementService.get(accountId);
        if (account.getVersion() != accountVersion) {
            throw new AccountConcurrentModificationException("Account current version " + account.getVersion() + " expected by client " + accountVersion);
        }

        LOG.info("Updating account {} balance to {}{}", accountId, account.getCurrency(), amountAsString);
        accountManagementService.update(accountId, accountVersion, amount);
        LOG.info("Updated account {} balance to {}{}", accountId, account.getCurrency(), amountAsString);
    }

    @Override
    public AccountDescription get(String accountId) throws AccountNotFoundException, ApplicationDataAccessError {
        /// validate input parameters
        ValidationUtils.isAccountIdValid(accountId);

        /// fetch data from database
        final Account account = accountManagementService.get(accountId);

        /// convert entity for caller layer
        final AccountDescription accountDescription = new AccountDescription();
        accountDescription.setBalance(account.getBalance().toString()); // TODO formatting shall be done not here!
        accountDescription.setCurrency(account.getCurrency());
        accountDescription.setAccountId(accountId);
        accountDescription.setVersion(account.getVersion());
        return accountDescription;
    }

    private void isCurrencyCodeValid(String currency) {
        if (!exchangeRateService.isCurrencyValid(currency)) {
            throw new IllegalArgumentException("Currency is not known " + currency);
        }
    }

}
