package com.revolut.hometask.app.business.memory;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.revolut.hometask.app.business.ClockService;
import com.revolut.hometask.app.errors.AccountConcurrentModificationException;
import com.revolut.hometask.app.errors.NoSufficientFundsException;
import com.revolut.hometask.app.errors.TransferAlreadyProcessedException;

import java.math.BigDecimal;

/**
 * This is JUST FOR FUN IMPLEMENTATION! <br/>
 * THIS IS NOT GOING TO WORK! <br/>
 * <p>
 * I just decided to have a look if i can make something functional.
 */
@Singleton
public class InMemoryLockingStorage extends InMemoryBaseHandler<InMemoryLockingStorage.Account> {

    private final String INTERNAL_TRANSFER_ID_PREFIX = "INTERNAL.";


    @Inject
    public InMemoryLockingStorage(ClockService clockService) {
        super(clockService);
    }

    @Override
    protected Account createAccount(String accountId, String currency) {
        return new Account(accountId, currency);
    }

    @Override
    protected void transferMoneyAction(String currency, long transactionTime, String clientTransferId,
                                       AccountInternal src, AccountInternal dst, BigDecimal fromAccountAmount,
                                       BigDecimal toAccountAmount) throws NoSufficientFundsException, TransferAlreadyProcessedException {
        /// find hierarchical lock order, first try to lock account with smaller
        // account id, lexicographical, and then bigger account id
        AccountInternal firstLock = (src.getId().compareToIgnoreCase(dst.getId()) < 0) ? src : dst;
        AccountInternal secondLock = (src.getId().compareToIgnoreCase(dst.getId()) < 0) ? dst : src;

        /// perform transfer logic
        /// synchronization logic can be tentatively changed to different sort of locks
        /// yet i see no reason to do that right now :)
        synchronized (firstLock) {
            synchronized (secondLock) {

                /// append transfer transaction to both accounts
                final TransferIntenal srcTx = src.createTransferState(fromAccountAmount, currency, src, dst, transactionTime, clientTransferId);
                final TransferIntenal dstTx = dst.createTransferState(toAccountAmount, currency, src, dst, transactionTime, null);

                /// verify transfer was never executed before
                if (!isTransferReferenceUniq(clientTransferId, srcTx.getNext())) {
                    throw new TransferAlreadyProcessedException(src.getId(), clientTransferId);
                }

                /// ensure we have enough amount
                if (srcTx.getBalance().compareTo(BigDecimal.ZERO) < 0) {
                    throw new NoSufficientFundsException(src.getId(), fromAccountAmount.toString(), src.getCurrency());
                }

                /// finalize money transfer, it's safe here to 'commit' transaction
                src.updateTransferHistory(srcTx);
                dst.updateTransferHistory(dstTx);
                /// hypothetically it(assignments) above should never fail, but who knows!
                /// one can go and get bytecode, but then it's up to JIT to decide on real code that will work here
                /// thus we can theoretically assume last two assignments will never cause an exception
            }
        }
    }

    @Override
    protected void updateAccountBalance(AccountInternal src, long accountVersion, BigDecimal amount) throws AccountConcurrentModificationException {
        synchronized (src) {
            if (src.getLastUpdated() != accountVersion) {
                throw new AccountConcurrentModificationException("Concurrent modification");
            }
            final long when = clockService.now();
            final String clientTransferId = INTERNAL_TRANSFER_ID_PREFIX + when;
            final BigDecimal balanceDifference = amount.subtract(src.getBalance());
            TransferIntenal forceSet = src.createTransferState(balanceDifference, src.getCurrency(), null, src, when, clientTransferId);
            src.updateTransferHistory(forceSet);
        }
    }

    @Override
    protected TransferIntenal getTransfersFromAccount(String account) {
        return accounts.get(account).transfers;
    }

    protected static class Account extends AccountInternal {

        /**
         * keep it volatile, as we cannot promise another thread would go route of syncing over the account
         * entity.
         */
        private volatile TransferIntenal transfers;

        protected Account(String id, String currency) {
            super(id, currency);
            this.transfers = new TransferIntenal(BigDecimal.ZERO, BigDecimal.ZERO, currency, null, null, 0, null, null);
        }

        public BigDecimal getBalance() {
            return this.transfers.getBalance();
        }

        @Override
        public boolean updateTransferHistory(TransferIntenal transfer) {
            this.transfers = transfer;
            return true;
        }

        @Override
        public long getLastUpdated() {
            return transfers.getWhen();
        }

        public TransferIntenal createTransferState(BigDecimal amountToTransfer, String currency,
                                                   AccountInternal srcAccount, AccountInternal dstAccount,
                                                   long when, String clientTransferId) {
            return new TransferIntenal(amountToTransfer, getBalance().add(amountToTransfer), currency,
                    srcAccount, dstAccount, when, clientTransferId, this.transfers);
        }

    }

}
