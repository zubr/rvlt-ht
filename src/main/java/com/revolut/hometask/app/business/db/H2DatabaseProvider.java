package com.revolut.hometask.app.business.db;

import com.google.inject.Provider;
import com.google.inject.Provides;
import org.h2.jdbcx.JdbcConnectionPool;

public class H2DatabaseProvider implements Provider<JdbcConnectionPool> {

    @Provides
    public JdbcConnectionPool getConnectionPool() {
        return JdbcConnectionPool.create("jdbc:h2:./transferdatabase;" +
                        "INIT=create schema if not exists test" +
                        "\\;runscript from 'classpath:scripts/create.sql'",
                "sa", "sa");
    }

    @Override
    public JdbcConnectionPool get() {
        return getConnectionPool();
    }
}
