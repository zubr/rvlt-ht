package com.revolut.hometask.app.rest;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.MalformedJsonException;
import com.revolut.hometask.app.errors.*;
import com.revolut.hometask.app.rest.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.RouteGroup;
import spark.Spark;

import java.util.List;
import java.util.stream.Collectors;

public class RoutesV1 {

    private final static Logger LOG = LoggerFactory.getLogger(RoutesV1.class.getCanonicalName());

    static RouteGroup coreRestApiV1Routes(AccountManagementEndpoint accountManagementEndpoint,
                                          TransferManagementEndpoint transferManagementEndpoint,
                                          TransferQueryEndpoint transferQueryEndpoint,
                                          Gson gson) {
        return () -> {
            /// exception error handling
            attachExceptionHandling(gson);

            /// rest endpoints
            /// account management
            Spark.path("/account", () -> {
                Spark.post("", ((request, response) -> {
                    final AccountCreateInput accountCreateInput = gson.fromJson(request.body(), AccountCreateInput.class);
                    accountManagementEndpoint.create(accountCreateInput.getAccountId(), accountCreateInput.getCurrency());
                    response.status(201);
                    return "OK";
                }));
                Spark.put("/:accountId", ((request, response) -> {
                    final String sourceAccount = request.params(":accountId");

                    final AccountUpdateAmountInput accountCreateInput = gson.fromJson(request.body(), AccountUpdateAmountInput.class);
                    accountManagementEndpoint.update(sourceAccount, accountCreateInput.getVersion(), accountCreateInput.getAmount());
                    response.status(200);
                    return "OK";
                }));
                Spark.get("/:accountId", ((request, response) -> {
                    final String accountIdParameterValue = request.params(":accountId");
                    AccountDescription accountDescription = accountManagementEndpoint.get(accountIdParameterValue);

                    response.status(200);
                    response.type("application/json");
                    AccountDescriptionOutput output = new AccountDescriptionOutput(accountDescription);
                    return gson.toJson(output);
                }));

                /// transfer management
                Spark.get("/:accountId/transfer", ((request, response) -> {
                    final String sourceAccount = request.params(":accountId");

                    LOG.debug("Get transfers for {}", sourceAccount);

                    final List<TransferDescription> transfersForAccount = transferQueryEndpoint.getTransfersForAccount(sourceAccount);
                    response.status(200);
                    List<TransferDescriptionOutput> responseList = transfersForAccount.stream()
                            .map(TransferDescriptionOutput::new).collect(Collectors.toList());
                    response.type("application/json");
                    return gson.toJson(responseList);
                }));
                Spark.post("/:accountId/transfer", ((request, response) -> {
                    final String sourceAccount = request.params(":accountId");
                    final MoneyTransferInput moneyTransferInput = gson.fromJson(request.body(), MoneyTransferInput.class);

                    final String destinationAccount = moneyTransferInput.getDestinationAccount();
                    final String amount = moneyTransferInput.getAmount();
                    final String currency = moneyTransferInput.getCurrency();
                    final String clientGeneratedTransferId = moneyTransferInput.getId();

                    transferManagementEndpoint.transfer(sourceAccount, destinationAccount, amount, currency, clientGeneratedTransferId);
                    response.status(201);
                    return "OK";
                }));
            });
        };
    }

    private static void attachExceptionHandling(Gson gson) {
        Spark.exception(ApplicationException.class, (e, request, response) -> {
            LOG.warn("Error while processing request.", e);

            final ErrorDescription errorDescription = new ErrorDescription();
            errorDescription.setDescription(e.getMessage());
            final String responseBody = gson.toJson(errorDescription);
            response.body(responseBody);
            response.type("application/json");
            response.status(400);
        });

        Spark.exception(ApplicationDataAccessError.class, (e, request, response) -> {
            LOG.warn("Error while processing request.", e);

            final ErrorDescription errorDescription = new ErrorDescription();
            errorDescription.setDescription("Internal server error. Please try again later.");
            final String responseBody = gson.toJson(errorDescription);
            response.body(responseBody);
            response.type("application/json");
            response.status(500);
        });

        Spark.exception(AccountConcurrentModificationException.class, (e, request, response) -> {
            LOG.warn("Error while processing request.", e);

            final ErrorDescription errorDescription = new ErrorDescription();
            errorDescription.setDescription("Client account state is stale. Please refresh data and try operation again.");
            final String responseBody = gson.toJson(errorDescription);
            response.body(responseBody);
            response.type("application/json");
            response.status(409);
        });

        Spark.exception(AccountNotFoundException.class, (e, request, response) -> {
            LOG.warn("Error while processing request.", e);

            final ErrorDescription errorDescription = new ErrorDescription();
            /// i do understand we can use formatting here, and even more here
            /// we can have translated and language specific messages to be sent to client
            errorDescription.setDescription("Error occurred while processing request - account with id = " + e.getAccountId() + " was not found.");
            final String responseBody = gson.toJson(errorDescription);
            response.status(404);
            response.type("application/json");
            response.body(responseBody);
        });

        Spark.exception(AccountAlreadyExistsException.class, (e, request, response) -> {
            LOG.warn("Error while processing request.", e);

            final ErrorDescription errorDescription = new ErrorDescription();
            errorDescription.setDescription("Error occurred while processing request - account with id = " + e.getAccountId() + " already exists.");
            final String responseBody = gson.toJson(errorDescription);
            response.status(409);
            response.type("application/json");
            response.body(responseBody);
        });

        Spark.exception(NoSufficientFundsException.class, (e, request, response) -> {
            LOG.warn("Error while processing request.", e);

            final ErrorDescription errorDescription = new ErrorDescription();
            errorDescription.setDescription("Money transfer operation declined. Source account with id = " + e.getSrcAccountId() +
                    " does not have sufficient balance to complete operation. Required at least " + e.getAmount() + e.getAmountCurrency() + " (includes fees).");
            final String responseBody = gson.toJson(errorDescription);
            response.status(400);
            response.type("application/json");
            response.body(responseBody);
        });

        Spark.exception(TransferAlreadyProcessedException.class, (e, request, response) -> {
            LOG.warn("Error while processing request.", e);

            final ErrorDescription errorDescription = new ErrorDescription();
            errorDescription.setDescription("Money transfer request has been already processed for account id = "
                    + e.getSrcAccountId() + " and transfer id = " + e.getClientTransferId());
            final String responseBody = gson.toJson(errorDescription);
            response.status(409);
            response.type("application/json");
            response.body(responseBody);
        });

        Spark.exception(IllegalArgumentException.class, (e, request, response) -> {
            LOG.warn("Error while processing request.", e);

            final ErrorDescription errorDescription = new ErrorDescription();
            errorDescription.setDescription(e.getMessage());
            final String responseBody = gson.toJson(errorDescription);
            response.status(400);
            response.type("application/json");
            response.body(responseBody);
        });

        Spark.exception(NumberFormatException.class, (e, request, response) -> {
            LOG.warn("Error while processing request.", e);

            final ErrorDescription errorDescription = new ErrorDescription();
            errorDescription.setDescription(e.getMessage());
            final String responseBody = gson.toJson(errorDescription);
            response.status(400);
            response.type("application/json");
            response.body(responseBody);
        });

        Spark.exception(JsonSyntaxException.class, (e, request, response) -> {
            LOG.warn("Error while processing request.", e);

            final ErrorDescription errorDescription = new ErrorDescription();
            if (e.getCause() != null
                    && !(e.getCause() instanceof IllegalStateException)
                    && !(e.getCause() instanceof MalformedJsonException)
            ) {
                errorDescription.setDescription(e.getCause().getMessage());
                response.status(400);
            } else {
                errorDescription.setDescription(e.getMessage());
                response.status(418);
            }

            final String responseBody = gson.toJson(errorDescription);
            response.type("application/json");
            response.body(responseBody);
        });
    }
}
