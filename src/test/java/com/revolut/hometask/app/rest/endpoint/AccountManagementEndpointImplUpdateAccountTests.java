package com.revolut.hometask.app.rest.endpoint;

import com.revolut.hometask.app.business.Account;
import com.revolut.hometask.app.business.AccountManagementService;
import com.revolut.hometask.app.business.ExchangeRateService;
import com.revolut.hometask.app.business.MoneyTransferService;
import com.revolut.hometask.app.errors.AccountConcurrentModificationException;
import com.revolut.hometask.app.errors.ApplicationException;
import com.revolut.hometask.app.rest.impl.AccountManagementEndpointImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class AccountManagementEndpointImplUpdateAccountTests {

    private final String accountId = "junit-account";

    private final long correctModificationTime = 123456L;

    private final String correctAmount = "1234.56";

    @InjectMocks
    private AccountManagementEndpointImpl service;

    @Mock
    private AccountManagementService accountManagementService;

    @Mock
    private ExchangeRateService exchangeRateService;

    @Mock
    private MoneyTransferService moneyTransferService;

    @Test
    public void updateAllIsOk() throws ApplicationException {
        final Account acc = new Account();
        acc.setVersion(correctModificationTime);
        acc.setAccountId(accountId);
        when(accountManagementService.get(eq(accountId))).thenReturn(acc);

        service.update(accountId, correctModificationTime, correctAmount);

        verify(accountManagementService, Mockito.times(1)).update(eq(accountId), eq(correctModificationTime), eq(new BigDecimal(correctAmount)));
    }

    @Test(expected = AccountConcurrentModificationException.class)
    public void udpate_concurrent_modification() throws ApplicationException {
        final Account acc = new Account();
        acc.setVersion(correctModificationTime);
        acc.setAccountId(accountId);
        when(accountManagementService.get(eq(accountId))).thenReturn(acc);

        service.update(accountId, correctModificationTime + 1, correctAmount);
    }

}
