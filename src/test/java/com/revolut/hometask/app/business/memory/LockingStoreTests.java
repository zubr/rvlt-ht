package com.revolut.hometask.app.business.memory;

import com.revolut.hometask.app.business.ClockService;
import org.junit.Before;
import org.mockito.Mockito;

public class LockingStoreTests extends ForFunlementationBaseScenarios {

    @Before
    public void prepare() {
        clockService = Mockito.mock(ClockService.class);
        service = new InMemoryLockingStorage(clockService);
    }

}
