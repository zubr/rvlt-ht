package com.revolut.hometask.app.business.db;

import org.h2.jdbcx.JdbcConnectionPool;

public class TestDatabaseConnectionProvider {

    public JdbcConnectionPool getConnectionPool() {
        return JdbcConnectionPool.create("jdbc:h2:./test;" +
                        "INIT=create schema if not exists test" +
                        "\\;runscript from 'classpath:scripts/create.sql'",
                "sa", "sa");
    }

}
