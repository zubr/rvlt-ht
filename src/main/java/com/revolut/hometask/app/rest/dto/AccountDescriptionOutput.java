package com.revolut.hometask.app.rest.dto;

import com.revolut.hometask.app.rest.AccountDescription;

public class AccountDescriptionOutput {

    private String accountId;

    private String currency;

    private String balance;

    private long version;

    public AccountDescriptionOutput(AccountDescription desc) {
        accountId = desc.getAccountId();
        currency = desc.getCurrency();
        balance = desc.getBalance();
        version = desc.getVersion();
    }

    public String getAccountId() {
        return accountId;
    }

    public String getCurrency() {
        return currency;
    }

    public String getBalance() {
        return balance;
    }

    public long getVersion() {
        return version;
    }
}
