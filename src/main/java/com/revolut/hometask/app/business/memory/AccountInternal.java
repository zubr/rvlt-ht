package com.revolut.hometask.app.business.memory;

import java.math.BigDecimal;

abstract class AccountInternal {

    private final String id;

    private final String currency;

    protected AccountInternal(String id, String currency) {
        this.id = id;
        this.currency = currency;
    }

    public String getId() {
        return id;
    }

    public abstract BigDecimal getBalance();

    public String getCurrency() {
        return currency;
    }

    /**
     * Implementations shall be in contract with the outer keeper logic.
     * Some implementations might require us to have absolute safety from any
     * sort of failures, some might be safe in terms of exceptions.
     * @param transfer latest transfer object
     * @return whether operation was successful or not
     */
    public abstract boolean updateTransferHistory(TransferIntenal transfer);

    public abstract long getLastUpdated();

    public abstract TransferIntenal createTransferState(BigDecimal amountToTransfer, String currency,
                                               AccountInternal srcAccount, AccountInternal dstAccount,
                                               long when, String clientTransactionId);
}
