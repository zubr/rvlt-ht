package com.revolut.hometask.app.rest.dto;

import com.revolut.hometask.app.rest.TransferDescription;

public class TransferDescriptionOutput {

    private String accountSourceId;

    private String accountDestinationId;

    private String amount;

    private String currency;

    private long when;

    public TransferDescriptionOutput(TransferDescription desc) {
        accountSourceId = desc.getAccountSourceId();
        accountDestinationId = desc.getAccountDestinationId();

        amount = desc.getAmount();
        currency = desc.getCurrency();

        when = desc.getWhen();
    }

    public String getAccountSourceId() {
        return accountSourceId;
    }

    public String getAccountDestinationId() {
        return accountDestinationId;
    }

    public String getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public long getWhen() {
        return when;
    }
}
