package com.revolut.hometask.app;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.revolut.hometask.app.business.*;
import com.revolut.hometask.app.business.db.H2AccountService;
import com.revolut.hometask.app.business.db.H2DatabaseProvider;
import com.revolut.hometask.app.business.db.H2TransferMoneyService;
import com.revolut.hometask.app.business.db.H2TransferQueryService;
import com.revolut.hometask.app.business.memory.InMemoryLockFreeStorage;
import com.revolut.hometask.app.rest.AccountManagementEndpoint;
import com.revolut.hometask.app.rest.TransferManagementEndpoint;
import com.revolut.hometask.app.rest.TransferQueryEndpoint;
import com.revolut.hometask.app.rest.impl.AccountManagementEndpointImpl;
import com.revolut.hometask.app.rest.RestEndpointConfig;
import com.revolut.hometask.app.rest.impl.TransferManagementEndpointImpl;
import com.revolut.hometask.app.rest.impl.TransferQueryEndpointImpl;
import org.h2.jdbcx.JdbcConnectionPool;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public abstract class TransferApplicationGuice extends AbstractModule {

    static class H2BackedApplication extends TransferApplicationGuice {
        void configStorage() {
            bind(JdbcConnectionPool.class).toProvider(H2DatabaseProvider.class);
            bind(AccountManagementService.class).to(H2AccountService.class).in(Singleton.class);
            bind(MoneyTransferService.class).to(H2TransferMoneyService.class).in(Singleton.class);
            bind(TransferQueryService.class).to(H2TransferQueryService.class).in(Singleton.class);
        }
    }

    static class InMemoryLockFreeApplication extends TransferApplicationGuice {
        void configStorage() {
            bind(AccountManagementService.class).to(InMemoryLockFreeStorage.class).in(Singleton.class);
            bind(MoneyTransferService.class).to(InMemoryLockFreeStorage.class).in(Singleton.class);
            bind(TransferQueryService.class).to(InMemoryLockFreeStorage.class).in(Singleton.class);
        }
    }

    static class InMemoryLockingApplication extends TransferApplicationGuice {
        void configStorage() {
            bind(AccountManagementService.class).to(InMemoryLockFreeStorage.class).in(Singleton.class);
            bind(MoneyTransferService.class).to(InMemoryLockFreeStorage.class).in(Singleton.class);
            bind(TransferQueryService.class).to(InMemoryLockFreeStorage.class).in(Singleton.class);
        }
    }

    @Override
    protected void configure() {

        // utility stuff
        bind(ClockService.class).toInstance(new ClockService() {
            @Override
            public long now() {
                return System.currentTimeMillis();
            }
        });

        bind(ExchangeRateService.class).toInstance(new ExchangeRateService() {
            // safe to have standard set
            private Set<String> availableCurrencies = new HashSet<>(Arrays.asList("USD", "GBP", "EUR", "SEK"));

            @Override
            public BigDecimal convertAmountBetweenCurrencies(String from, String to, BigDecimal amount, long transactionTime) {
                return amount;
            }

            @Override
            public boolean isCurrencyValid(String currency) {
                return availableCurrencies.contains(currency.toUpperCase());
            }
        });

        /// Storage configuration
        configStorage();

        // endpoints
        bind(AccountManagementEndpoint.class).to(AccountManagementEndpointImpl.class).in(Singleton.class);
        bind(TransferManagementEndpoint.class).to(TransferManagementEndpointImpl.class).in(Singleton.class);
        bind(TransferQueryEndpoint.class).to(TransferQueryEndpointImpl.class).in(Singleton.class);

        // app config
        bind(RestEndpointConfig.class).in(Singleton.class);
    }

    abstract void configStorage();
}
