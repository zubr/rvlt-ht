package com.revolut.hometask.app.business;

import com.revolut.hometask.app.errors.ApplicationDataAccessError;

import java.util.List;

public interface TransferQueryService {

    /**
     * Returns all transfers for account (including in and out)
     * @param account account id
     * @return list of transfers
     */
    List<Transfer> getTransfersForAccount(String account) throws ApplicationDataAccessError;

    /**
     * Returns all transfers for account, where passed account is target
     * @param account account id
     * @return list of transfers
     */
    List<Transfer> getTransfersIntoAccount(String account) throws ApplicationDataAccessError;

    /**
     * Returns all transfers for account, where passed account is source
     * @param account account id
     * @return list of transfers
     */
    List<Transfer> getTransfersPaymentsFromAccount(String account) throws ApplicationDataAccessError;

}
