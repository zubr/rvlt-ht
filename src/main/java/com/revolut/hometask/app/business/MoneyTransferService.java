package com.revolut.hometask.app.business;

import com.revolut.hometask.app.errors.*;

import java.math.BigDecimal;

public interface MoneyTransferService {

    /**
     * Merthod performs actual money transfer between two accounts.
     * @param src source account
     * @param srcDiff source account money to be added
     * @param dst destination account
     * @param dstDiff destination account money to be added
     * @param amount amount of money to transfer
     * @param currency transfer currency
     * @param clientTransferId client generated unique identified of TX
     */
    void transfer(Account src, BigDecimal srcDiff, Account dst, BigDecimal dstDiff,
                  BigDecimal amount, String currency, String clientTransferId)
            throws ApplicationDataAccessError, TransferAlreadyProcessedException,
            NoSufficientFundsException, AccountConcurrentModificationException;

}
