package com.revolut.hometask.app.business;

import java.math.BigDecimal;

public interface ExchangeRateService {

    /**
     * Converts amount of given currency (from) into new currency (to)
     * @param from initial currency
     * @param to target currency
     * @param amount amount to be converted
     * @param transactionTime transaction startup time
     * @return converted amount
     */
    BigDecimal convertAmountBetweenCurrencies(String from, String to, BigDecimal amount, long transactionTime);

    /**
     * Tests if currency is known to exchange rate service or not
     * @param currency currency code to check
     * @return true IFF currency code is known to exchange rate service
     */
    boolean isCurrencyValid(String currency);

}
