package com.revolut.hometask.app.business.memory;

import com.revolut.hometask.app.business.ClockService;
import com.revolut.hometask.app.business.GenericTestMoneyTransfer;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mockito;

@RunWith(Parameterized.class)
public class CommonLockingSetTests extends GenericTestMoneyTransfer {

    public CommonLockingSetTests() {

        final InMemoryLockingStorage inMemoryLockFreeStorage = new InMemoryLockingStorage(Mockito.mock(ClockService.class));

        accountManagementService = inMemoryLockFreeStorage;
        transferService = inMemoryLockFreeStorage;
    }
}
