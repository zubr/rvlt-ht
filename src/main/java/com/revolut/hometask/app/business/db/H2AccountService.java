package com.revolut.hometask.app.business.db;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.revolut.hometask.app.business.Account;
import com.revolut.hometask.app.business.AccountManagementService;
import com.revolut.hometask.app.errors.*;
import org.h2.jdbc.JdbcSQLIntegrityConstraintViolationException;
import org.h2.jdbcx.JdbcConnectionPool;

import java.math.BigDecimal;
import java.sql.*;

@Singleton
public class H2AccountService implements AccountManagementService {

    private final JdbcConnectionPool connectionPool;

    private final String INSERT_ACCOUNT_STMT = "insert into account(account_id, currency, balance, created, version) " +
            "values (?, ?, ?, CURRENT_TIMESTAMP(), 0)";

    private final String GET_ACCOUNT_STMT = "select account_id, currency, balance, created, version from account where account_id = ?";

    private final String UPDATE_ACCOUNT_AMOUNT_STMT = "update account set balance = ?, version = ? where account_id = ? and version = ?";

    @Inject
    public H2AccountService(JdbcConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
    }

    @Override
    public void create(String accountId, String currency) throws AccountAlreadyExistsException, ApplicationDataAccessError {
        try (final Connection connection = connectionPool.getConnection();
             final PreparedStatement ps = connection.prepareStatement(INSERT_ACCOUNT_STMT)) {

            connection.setAutoCommit(false);

            ps.setString(1, accountId);
            ps.setString(2, currency);
            ps.setString(3, "0.00");

            final int count = ps.executeUpdate();
            if (count != 1) {
                throw new AccountAlreadyExistsException(accountId);
            }
            connection.commit();
        } catch (JdbcSQLIntegrityConstraintViolationException e) {
            throw new AccountAlreadyExistsException(accountId);
        } catch (SQLException e) {
            throw new ApplicationDataAccessError(e);
        }
    }

    @Override
    public void update(String accountId, long accountVersion, BigDecimal amount) throws ApplicationDataAccessError, AccountConcurrentModificationException {
        try (final Connection connection = connectionPool.getConnection();
             final PreparedStatement ps = connection.prepareStatement(UPDATE_ACCOUNT_AMOUNT_STMT)) {

            connection.setAutoCommit(false);

            ps.setString(1, amount.toString());
            ps.setLong(2, accountVersion + 1);
            ps.setString(3, accountId);
            ps.setLong(4, accountVersion);


            final int count = ps.executeUpdate();
            if (count != 1) {
                throw new AccountConcurrentModificationException(accountId);
            }
            connection.commit();
        } catch (SQLException e) {
            throw new ApplicationDataAccessError(e);
        }
    }

    @Override
    public Account get(String accountId) throws AccountNotFoundException, ApplicationDataAccessError {
        try (final Connection connection = connectionPool.getConnection();
             final PreparedStatement ps = connection.prepareStatement(GET_ACCOUNT_STMT)) {

            connection.setAutoCommit(false);

            ps.setString(1, accountId);

            try (final ResultSet resultSet = ps.executeQuery()) {
                if (resultSet.next()) {
                    return parseAccountFromResultSet(accountId, resultSet);
                } else {
                    throw new AccountNotFoundException(accountId);
                }
            }
        } catch (SQLException e) {
            throw new ApplicationDataAccessError(e);
        }
    }

    private Account parseAccountFromResultSet(String accountId, ResultSet resultSet) throws SQLException {
        final Account account = new Account();
        account.setAccountId(accountId);
        account.setCurrency(resultSet.getString(2));
        account.setBalance(new BigDecimal(resultSet.getString(3)));
        account.setCreatedTime(resultSet.getTimestamp(4).getTime());
        account.setVersion(resultSet.getLong(5));
        return account;
    }
}
