
create table if not exists account(account_id varchar(256) primary key, currency varchar(16), balance varchar(256), created timestamp, version int);

create table if not exists transfer(src_account_id varchar(256), dst_account_id varchar(256), currency varchar(16), amount varchar(256), created timestamp, client_id varchar(64));

alter table transfer add constraint if not exists uniq__transfer_client_generated_id unique (src_account_id, client_id);

