package com.revolut.hometask.app;

import com.google.inject.Guice;
import com.revolut.hometask.app.rest.RestEndpointConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TransferApplicationEntryPoint {

    private final static Logger LOG = LoggerFactory.getLogger(TransferApplicationEntryPoint.class.getCanonicalName());

    public static void main(String[] args) {

        int portNumber = 8080;
        if (args.length > 0) {
            portNumber = Integer.parseInt(args[0]);
        }

        String storageType = "h2";
        if (args.length > 1) {
            storageType = args[1];
        }

        LOG.info("Starting web server on port {} with storage {}", portNumber, storageType);

        Guice.createInjector(get(storageType))
                .getInstance(RestEndpointConfig.class)
                .run(portNumber);
    }

    protected static TransferApplicationGuice get(String storageType) {
        if ("inMemoryLocking".equalsIgnoreCase(storageType)) {
            return new TransferApplicationGuice.InMemoryLockingApplication();
        }
        if ("inMemoryLockFree".equalsIgnoreCase(storageType)) {
            return new TransferApplicationGuice.InMemoryLockFreeApplication();
        } else {
            return new TransferApplicationGuice.H2BackedApplication();
        }
    }

}
