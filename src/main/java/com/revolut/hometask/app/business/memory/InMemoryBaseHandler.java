package com.revolut.hometask.app.business.memory;

import com.google.inject.Inject;
import com.revolut.hometask.app.business.*;
import com.revolut.hometask.app.errors.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * This is JUST FOR FUN IMPLEMENTATION! <br/>
 * THIS IS NOT GOING TO WORK! <br/>
 * <p>
 * I just decided to have a look if i can make something functional.
 * <p>
 * It does break SRP! It does break everything! :)
 */
public abstract class InMemoryBaseHandler<T extends AccountInternal>
        implements AccountManagementService, MoneyTransferService, TransferQueryService {

    protected final ClockService clockService;

    protected final ConcurrentHashMap<String, T> accounts = new ConcurrentHashMap<>();

    @Inject
    public InMemoryBaseHandler(ClockService clockService) {
        this.clockService = clockService;
    }

    protected abstract T createAccount(String accountId, String currency);

    @Override
    public void create(String accountId, String currency) throws AccountAlreadyExistsException {
        final T value = createAccount(accountId, currency);

        final Object account = accounts.putIfAbsent(accountId, value);
        if (account != null) {
            throw new AccountAlreadyExistsException(accountId);
        }
    }

    @Override
    public Account get(String accountId) throws AccountNotFoundException {
        final AccountInternal accountInternal = getAccount(accountId);
        final Account account = new Account();
        final long accountVersion = accountInternal.getLastUpdated();

        account.setCreatedTime(accountVersion);
        account.setBalance(accountInternal.getBalance());
        account.setCurrency(accountInternal.getCurrency());
        account.setAccountId(accountId);
        account.setVersion(accountVersion);
        return account;
    }

    @Override
    public void update(String accountId, long accountVersion, BigDecimal amount) throws ApplicationDataAccessError, AccountConcurrentModificationException {
        AccountInternal src = accounts.get(accountId);
        updateAccountBalance(src, accountVersion, amount);
    }

    @Override
    public void transfer(Account srcAcc, BigDecimal srcDiff, Account dstAcc, BigDecimal dstDiff,
                         BigDecimal amount, String currency, String clientTransferId)
            throws ApplicationDataAccessError, TransferAlreadyProcessedException,
            NoSufficientFundsException, AccountConcurrentModificationException {

        /// get the time, and use it later to get conversion rate for specific time
        final long transactionTime = clockService.now();

        AccountInternal src = accounts.get(srcAcc.getAccountId());
        AccountInternal dst = accounts.get(dstAcc.getAccountId());

        /// execute real action
        transferMoneyAction(currency, transactionTime, clientTransferId, src, dst, srcDiff, dstDiff);
    }


    @Override
    public List<Transfer> getTransfersForAccount(String account) throws ApplicationDataAccessError {
        TransferIntenal transfer = getTransfersFromAccount(account);
        List<Transfer> transferList = new ArrayList<>();
        while (transfer != null) {
            TransferIntenal transferNext = transfer.getNext();

            /// a bit stupid approach to handle the first dummy account initialization transaction
            /// one can think of making an inherited class that would be skipped here
            /// as it's just a marker of transaction
            if (transferNext != null) {
                final Transfer transferResponce = convertTransfer(transfer);
                transferList.add(transferResponce);
            }
            transfer = transferNext;
        }
        return transferList;
    }

    @Override
    public List<Transfer> getTransfersIntoAccount(String account) throws ApplicationDataAccessError {
        return getTransfersForAccount(account)
                .stream().filter(transfer -> account.equals(transfer.getAccountDestinationId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Transfer> getTransfersPaymentsFromAccount(String account) throws ApplicationDataAccessError {
        return getTransfersForAccount(account)
                .stream().filter(transfer -> account.equals(transfer.getAccountDestinationId()))
                .collect(Collectors.toList());
    }

    public void create(String accountId, String currency, BigDecimal amount) throws AccountAlreadyExistsException {
        final T value = createAccount(accountId, currency);
        final TransferIntenal transfer = value.createTransferState(amount, currency, null, value, 0, null);
        value.updateTransferHistory(transfer);

        final AccountInternal account = accounts.putIfAbsent(accountId, value);
        if (account != null) {
            throw new AccountAlreadyExistsException(accountId);
        }
    }

    protected abstract TransferIntenal getTransfersFromAccount(String account);

    protected Transfer convertTransfer(TransferIntenal transfer) {
        final Transfer transferResponse = new Transfer();
        transferResponse.setWhen(transfer.getWhen());
        transferResponse.setCurrency(transfer.getCurrency());
        transferResponse.setAmount(transfer.getAmount().toString());
        transferResponse.setAccountDestinationId(transfer.getDst().getId());

        /// handle the case when amount forcefully set to specific money
        if (transfer.getSrc() != null) {
            transferResponse.setAccountSourceId(transfer.getSrc().getId());
        }
        return transferResponse;
    }

    protected abstract void transferMoneyAction(String currency, long transactionTime, String clientTransferId,
                                                AccountInternal src, AccountInternal dst, BigDecimal fromAccountAmount,
                                                BigDecimal toAccountAmount)
            throws ApplicationDataAccessError, TransferAlreadyProcessedException, NoSufficientFundsException, AccountConcurrentModificationException;

    protected boolean isTransferReferenceUniq(String clientTransferId, TransferIntenal transfers) {
        while (transfers != null) {
            if (Objects.equals(clientTransferId, transfers.getClientTransactionId())) {
                return false;
            }
            transfers = transfers.getNext();
        }
        return true;
    }

    protected abstract void updateAccountBalance(AccountInternal src, long accountVersion, BigDecimal amount) throws ApplicationDataAccessError, AccountConcurrentModificationException;

    private T getAccount(String accountId) throws AccountNotFoundException {
        final T account = accounts.get(accountId);
        if (account == null) {
            throw new AccountNotFoundException(accountId);
        }
        return account;
    }

}
