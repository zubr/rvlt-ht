package com.revolut.hometask.app.rest.endpoint;

import com.revolut.hometask.app.business.AccountManagementService;
import com.revolut.hometask.app.business.ClockService;
import com.revolut.hometask.app.business.ExchangeRateService;
import com.revolut.hometask.app.business.MoneyTransferService;
import com.revolut.hometask.app.errors.ApplicationException;
import com.revolut.hometask.app.rest.impl.AccountManagementEndpointImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;


@RunWith(Parameterized.class)
public class AccountManagementEndpointImplCreateAccountFailureValidationTests {

    private static final String srcAccountId = "junit-account";

    private static final String currencyCodeEUR = "EUR";

    private static final String currencyCodeInvalid = "Invalid";

    private AccountManagementEndpointImpl service;

    private AccountManagementService accountManagementService;

    private ExchangeRateService exchangeRateService;

    private MoneyTransferService moneyTransferService;

    private ClockService clockService;

    @Before
    public void prepare() {
        accountManagementService = Mockito.mock(AccountManagementService.class);
        exchangeRateService = Mockito.mock(ExchangeRateService.class);
        moneyTransferService = Mockito.mock(MoneyTransferService.class);
        clockService = Mockito.mock(ClockService.class);

        when(exchangeRateService.isCurrencyValid(eq(currencyCodeEUR))).thenReturn(true);
        when(exchangeRateService.isCurrencyValid(eq(currencyCodeInvalid))).thenReturn(false);

        service = new AccountManagementEndpointImpl(accountManagementService, exchangeRateService);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        final String dummyAccountId = String.join("-", Collections.nCopies(10, srcAccountId));

        return Arrays.asList(new Object[][] {
                {srcAccountId, null},
                {srcAccountId, currencyCodeInvalid},
                {srcAccountId + " ", currencyCodeEUR},
                {" " + srcAccountId + " ", currencyCodeEUR},
                {" " + srcAccountId, currencyCodeEUR},
                {null, currencyCodeEUR},
                {" ", currencyCodeEUR},
                {dummyAccountId, currencyCodeEUR},
                {null, null}
        });
    }

    @Parameterized.Parameter(0)
    public String accountIdParameter;

    @Parameterized.Parameter(1)
    public String currencyParameter;

    @Test(expected = IllegalArgumentException.class)
    public void validate_account_creation_validation_logic() throws ApplicationException {
        service.create(accountIdParameter, currencyParameter);
    }

}
