package com.revolut.hometask.app.business.memory;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.revolut.hometask.app.business.ClockService;
import com.revolut.hometask.app.business.Transfer;
import com.revolut.hometask.app.errors.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

/**
 * This is JUST FOR FUN IMPLEMENTATION! <br/>
 * THIS IS NOT GOING TO WORK! <br/>
 * <p>
 * I just decided to have a look if i can make something functional.
 */
@Singleton
public class InMemoryLockFreeStorage extends InMemoryBaseHandler<InMemoryLockFreeStorage.Account> {

    private final String INTERNAL_TRANSFER_ID_PREFIX = "INTERNAL.";

    @Inject
    public InMemoryLockFreeStorage(ClockService clockService) {
        super(clockService);
    }

    @Override
    protected Account createAccount(String accountId, String currency) {
        return new Account(accountId, currency);
    }

    @Override
    protected void transferMoneyAction(String currency, long transactionTime, String clientTransferId,
                                       AccountInternal src, AccountInternal dst,
                                       BigDecimal fromAccountAmount,
                                       BigDecimal toAccountAmount)
            throws AccountConcurrentModificationException, TransferAlreadyProcessedException, NoSufficientFundsException {

        /// lets first decrease amount of money we have on source account
        /// it would mean that any tricky validation logic
        /// can be then avoided
        {
            Account acc = (Account) src;
            final TransferIntenal srcTx = src.createTransferState(fromAccountAmount, currency, src, dst, transactionTime, clientTransferId);

            if (!isTransferReferenceUniq(clientTransferId, srcTx.getNext())) {
                throw new TransferAlreadyProcessedException(src.getId(), clientTransferId);
            }

            if (srcTx.getBalance().compareTo(BigDecimal.ZERO) < 0) {
                throw new NoSufficientFundsException(src.getId(), fromAccountAmount.toString(), src.getCurrency());
            }
            /// update source account
            if (!acc.updateTransferHistory(srcTx)) {
                throw new AccountConcurrentModificationException("Concurrent modification exception");
            }
        }

        {
            Account acc = (Account) dst;
            TransferIntenal dstTx = dst.createTransferState(toAccountAmount, currency, src, dst, transactionTime, null);
            /// We shall try as hard as we can, we cannot simply give up here
            /// We've already transferred amount of money
            while (!acc.updateTransferHistory(dstTx)) {
                try {
                    Thread.sleep(500); // or yield, does not really matter much
                    // better sleep or yield depends on workload
                } catch (InterruptedException e) {
                }
                dstTx = dst.createTransferState(toAccountAmount, currency, src, dst, transactionTime, null);
            }

            /// side note here for reader, it's a tricky question, but we really
            /// can perform an update call back to source account and rollback operation
            /// how one can perform it? well pretty simple and safe - we are increasing
            /// money, not decreasing, thus account would always have sufficient amount of money
            /// i really played too much here
        }
    }

    @Override
    protected void updateAccountBalance(AccountInternal src, long accountVersion, BigDecimal amount) throws AccountConcurrentModificationException {
        Account acc = (Account) src;
        final long txTime = clockService.now();
        final String clientTransferId = INTERNAL_TRANSFER_ID_PREFIX + txTime;
        final BigDecimal balanceDifference = amount.subtract(src.getBalance());
        final TransferIntenal srcTx = src.createTransferState(balanceDifference, src.getCurrency(), null, src, txTime, clientTransferId);
        /// transaction was updated before our calls, and our state is stale
        if (srcTx.getNext().getWhen() != accountVersion) {
            throw new AccountConcurrentModificationException("Account object state stale.");
        }
        /// someone updated state of account, we cannot continue
        if (!acc.updateTransferHistory(srcTx)) {
            throw new AccountConcurrentModificationException("Concurrent modification.");
        }
    }

    @Override
    protected TransferIntenal getTransfersFromAccount(String account) {
        return accounts.get(account).transfers.get();
    }

    protected static class Account extends AccountInternal {

        private AtomicReference<TransferIntenal> transfers;

        protected Account(String id, String currency) {
            super(id, currency);
            this.transfers = new AtomicReference<>();
            final TransferIntenal transfer = new TransferIntenal(BigDecimal.ZERO, BigDecimal.ZERO, currency, null, null, 0, null, null);
            transfers.set(transfer);
        }

        public BigDecimal getBalance() {
            return this.transfers.get().getBalance();
        }

        @Override
        public boolean updateTransferHistory(TransferIntenal transfer) {
            return transfers.compareAndSet(transfer.getNext(), transfer);
        }

        @Override
        public long getLastUpdated() {
            return transfers.get().getWhen();
        }

        @Override
        public TransferIntenal createTransferState(BigDecimal amountToTransfer, String currency,
                                                   AccountInternal srcAccount, AccountInternal dstAccount,
                                                   long when, String clientTransferId) {
            return new TransferIntenal(amountToTransfer, getBalance().add(amountToTransfer), currency,
                    srcAccount, dstAccount, when, clientTransferId, this.transfers.get());
        }
    }
}
