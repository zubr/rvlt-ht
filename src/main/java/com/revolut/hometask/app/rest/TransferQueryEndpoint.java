package com.revolut.hometask.app.rest;

import com.revolut.hometask.app.errors.AccountNotFoundException;
import com.revolut.hometask.app.errors.ApplicationDataAccessError;

import java.util.List;

public interface TransferQueryEndpoint {

    /**
     * Returns all transfers for account (including in and out)
     * @param account account id
     * @return list of transfers
     */
    List<TransferDescription> getTransfersForAccount(String account) throws AccountNotFoundException, ApplicationDataAccessError;

    /**
     * Returns all transfers for account, where passed account is target
     * @param account account id
     * @return list of transfers
     */
    List<TransferDescription> getTransfersIntoAccount(String account) throws AccountNotFoundException, ApplicationDataAccessError;

    /**
     * Returns all transfers for account, where passed account is source
     * @param account account id
     * @return list of transfers
     */
    List<TransferDescription> getTransfersPaymentsFromAccount(String account) throws AccountNotFoundException, ApplicationDataAccessError;

}
