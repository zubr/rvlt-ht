package com.revolut.hometask.app.business.db;

import com.revolut.hometask.app.business.Account;
import com.revolut.hometask.app.business.ClockService;
import com.revolut.hometask.app.business.Transfer;
import com.revolut.hometask.app.errors.*;
import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.*;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class H2TransferQueryServiceTests {

    private static TestDatabaseConnectionProvider dbConnectionProvider;
    private static JdbcConnectionPool connectionPool;
    private static AtomicInteger counter = new AtomicInteger(0);

    private final String accountId = "h2-junit-account-id";
    private final String accountTransferId = accountId + "query.transfer";
    private final String currencyCodeEUR = "eur";
    private H2AccountService accountService;
    private H2TransferMoneyService transferMoneyService;
    private H2TransferQueryService transferQueryService;

    @BeforeClass
    public static void prepareDb() {
        dbConnectionProvider = new TestDatabaseConnectionProvider();
        connectionPool = dbConnectionProvider.getConnectionPool();
    }

    @AfterClass
    public static void clearDb() throws SQLException {
        connectionPool.getConnection().prepareStatement("drop all objects").execute();
    }

    private String generateAccountName() {
        return accountId + "." + counter.incrementAndGet();
    }

    @Before
    public void prepare() {
        accountService = new H2AccountService(connectionPool);
        transferQueryService = new H2TransferQueryService(connectionPool);
        transferMoneyService = new H2TransferMoneyService(connectionPool, new ClockService() {
            @Override
            public long now() {
                return System.currentTimeMillis();
            }
        });
    }

    @Test
    public void transferMoney_single_transaction() throws ApplicationException {
        final String testAccountId1 = generateAccountName();
        final String testAccountId2 = generateAccountName();

        accountService.create(testAccountId1, currencyCodeEUR);
        Account account = accountService.get(testAccountId1);
        accountService.update(testAccountId1, account.getVersion(), new BigDecimal("321.45"));

        accountService.create(testAccountId2, currencyCodeEUR);

        final BigDecimal transferAmount = new BigDecimal("0.45");
        transferMoneyService.transfer(accountService.get(testAccountId1), transferAmount,
                accountService.get(testAccountId2), transferAmount,
                transferAmount, "EUR", accountTransferId + System.currentTimeMillis());

        {
            final List<Transfer> transfersForAccount1 = transferQueryService.getTransfersForAccount(testAccountId1);
            Assert.assertNotNull(transfersForAccount1);
            Assert.assertEquals(1, transfersForAccount1.size());

        }

        {
            final List<Transfer> transfersForAccount2 = transferQueryService.getTransfersForAccount(testAccountId2);
            Assert.assertNotNull(transfersForAccount2);
            Assert.assertEquals(1, transfersForAccount2.size());

        }
    }

    @Test
    public void transferMoney_check_src_only_transaction() throws ApplicationException {
        final String testAccountId1 = generateAccountName();
        final String testAccountId2 = generateAccountName();

        accountService.create(testAccountId1, currencyCodeEUR);
        Account account = accountService.get(testAccountId1);
        accountService.update(testAccountId1, account.getVersion(), new BigDecimal("321.45"));

        accountService.create(testAccountId2, currencyCodeEUR);

        final BigDecimal transferAmount = new BigDecimal("0.45");
        transferMoneyService.transfer(accountService.get(testAccountId1), transferAmount,
                accountService.get(testAccountId2), transferAmount,
                transferAmount, "EUR", accountTransferId + System.currentTimeMillis());

        {
            final List<Transfer> transfersForAccount1 = transferQueryService.getTransfersPaymentsFromAccount(testAccountId1);
            Assert.assertNotNull(transfersForAccount1);
            Assert.assertEquals(1, transfersForAccount1.size());

        }

        {
            final List<Transfer> transfersForAccount2 = transferQueryService.getTransfersPaymentsFromAccount(testAccountId2);
            Assert.assertNotNull(transfersForAccount2);
            Assert.assertEquals(0, transfersForAccount2.size());

        }
    }

    @Test
    public void transferMoney_check_dst_only_transaction() throws ApplicationException {
        final String testAccountId1 = generateAccountName();
        final String testAccountId2 = generateAccountName();

        accountService.create(testAccountId1, currencyCodeEUR);
        Account account = accountService.get(testAccountId1);
        accountService.update(testAccountId1, account.getVersion(), new BigDecimal("321.45"));

        accountService.create(testAccountId2, currencyCodeEUR);

        final BigDecimal transferAmount = new BigDecimal("0.45");
        transferMoneyService.transfer(accountService.get(testAccountId1), transferAmount,
                accountService.get(testAccountId2), transferAmount,
                transferAmount, "EUR", accountTransferId + System.currentTimeMillis());

        {
            final List<Transfer> transfersForAccount1 = transferQueryService.getTransfersIntoAccount(testAccountId1);
            Assert.assertNotNull(transfersForAccount1);
            Assert.assertEquals(0, transfersForAccount1.size());

        }

        {
            final List<Transfer> transfersForAccount2 = transferQueryService.getTransfersIntoAccount(testAccountId2);
            Assert.assertNotNull(transfersForAccount2);
            Assert.assertEquals(1, transfersForAccount2.size());

        }
    }

}
