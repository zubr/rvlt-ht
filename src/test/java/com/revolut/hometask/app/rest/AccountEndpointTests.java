package com.revolut.hometask.app.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.revolut.hometask.app.errors.*;
import com.revolut.hometask.app.rest.dto.*;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class AccountEndpointTests extends AccountRESTEndpointTestsBase {

    private Gson gsonConverter;

    private CloseableHttpClient httpClient;

    @Before
    public void prepare() {
        final GsonBuilder gsonBuilder = new GsonBuilder();
        gsonConverter = gsonBuilder.create();

        httpClient = HttpClients.custom().build();
    }

    @Test
    public void verify_account_creation_call_passes_details_to_service() throws IOException, AccountAlreadyExistsException, ApplicationDataAccessError {
        final String accountName = "TEST_ACCT_0001";
        final String accountCurrency = "SEK";

        AccountCreateInput input = new AccountCreateInput();
        input.setAccountId(accountName);
        input.setCurrency(accountCurrency);
        final String accountCreateAsString = gsonConverter.toJson(input);

        HttpPost httpCall = new HttpPost("http://localhost:9090/rest/v1/account");
        httpCall.setEntity(new StringEntity(accountCreateAsString));
        httpCall.setHeader("Accept", "application/json");
        httpCall.setHeader("Content-type", "application/json");

        // verify http call is ok
        try (CloseableHttpResponse response = httpClient.execute(httpCall)) {
            final StatusLine responseStatus = response.getStatusLine();
            Assert.assertNotNull(responseStatus);
            Assert.assertEquals("Account created shall respond with http-created", 201, responseStatus.getStatusCode());
        }

        // make sure parameters are passed
        verify(accountManagementEndpoint, times(1)).create(eq(accountName), eq(accountCurrency));
    }

    @Test
    public void verify_account_update_call_passes_details_to_service() throws IOException, ApplicationException {
        final String accountName = "TEST_ACCT_0001";
        final String accountAmount = "12345.56";
        final long lastSeenTime = 12345667L;

        AccountUpdateAmountInput input = new AccountUpdateAmountInput();
        input.setAmount(accountAmount);
        input.setVersion(lastSeenTime);
        final String accountCreateAsString = gsonConverter.toJson(input);

        HttpPut httpCall = new HttpPut("http://localhost:9090/rest/v1/account/" + accountName);
        httpCall.setEntity(new StringEntity(accountCreateAsString));
        httpCall.setHeader("Accept", "application/json");
        httpCall.setHeader("Content-type", "application/json");

        // verify http call is ok
        try (CloseableHttpResponse response = httpClient.execute(httpCall)) {
            final StatusLine responseStatus = response.getStatusLine();
            Assert.assertNotNull(responseStatus);
            Assert.assertEquals("Account created shall respond with http-created", 200, responseStatus.getStatusCode());
        }

        // make sure parameters are passed
        verify(accountManagementEndpoint, times(1)).update(eq(accountName), eq(lastSeenTime), eq(accountAmount));
    }

    @Test
    public void verify_concurrent_modification_error_code() throws IOException, ApplicationException {
        final String accountName = "TEST_ACCT_0001";
        final String accountAmount = "12345.56";
        final long lastSeenTime = 12345667L;

        final String errorCode = accountAmount + accountAmount + lastSeenTime;
        doThrow(new AccountConcurrentModificationException(errorCode)) //
                .when(accountManagementEndpoint) //
                .update(eq(accountName), eq(lastSeenTime), eq(accountAmount));

        AccountUpdateAmountInput input = new AccountUpdateAmountInput();
        input.setAmount(accountAmount);
        input.setVersion(lastSeenTime);
        final String accountCreateAsString = gsonConverter.toJson(input);

        HttpPut httpCall = new HttpPut("http://localhost:9090/rest/v1/account/" + accountName);
        httpCall.setEntity(new StringEntity(accountCreateAsString));
        httpCall.setHeader("Accept", "application/json");
        httpCall.setHeader("Content-type", "application/json");

        // verify http call is ok
        try (CloseableHttpResponse response = httpClient.execute(httpCall)) {
            final StatusLine responseStatus = response.getStatusLine();
            Assert.assertNotNull(responseStatus);
            Assert.assertEquals("Account created shall respond with http-conflict", 409, responseStatus.getStatusCode());

            final InputStreamReader jsonStreamReader = new InputStreamReader(response.getEntity().getContent());
            final ErrorDescription errorDescription = gsonConverter.fromJson(jsonStreamReader, ErrorDescription.class);
            Assert.assertTrue(errorDescription.getDescription().contains("Client account state is stale"));
        }
    }

    @Test
    public void verify_account_database_error_converted_to_500() throws IOException, ApplicationException {
        final String accountName = "TEST_ACCT_0001";
        final String accountCurrency = "SEK";
        final String illegalArgumentString = "Illegal-argument-junit-test-check";

        doThrow(new ApplicationDataAccessError(illegalArgumentString)) //
                .when(accountManagementEndpoint) //
                .create(eq(accountName), anyString());

        AccountCreateInput input = new AccountCreateInput();
        input.setAccountId(accountName);
        input.setCurrency(accountCurrency);
        final String accountCreateAsString = gsonConverter.toJson(input);

        HttpPost httpCall = new HttpPost("http://localhost:9090/rest/v1/account");
        httpCall.setEntity(new StringEntity(accountCreateAsString));
        httpCall.setHeader("Accept", "application/json");
        httpCall.setHeader("Content-type", "application/json");

        // verify http call is ok
        try (CloseableHttpResponse response = httpClient.execute(httpCall)) {
            final StatusLine responseStatus = response.getStatusLine();
            Assert.assertNotNull(responseStatus);
            Assert.assertEquals("Account created shall respond with http-internal-server-error", 500, responseStatus.getStatusCode());

            final InputStreamReader jsonStreamReader = new InputStreamReader(response.getEntity().getContent());
            final ErrorDescription errorDescription = gsonConverter.fromJson(jsonStreamReader, ErrorDescription.class);
            Assert.assertTrue(errorDescription.getDescription().contains("Internal server error."));
        }
    }

    @Test
    public void verify_illegal_arguments_account_creation() throws IOException, AccountAlreadyExistsException, ApplicationDataAccessError {
        final String accountName = "TEST_ACCT_0001";
        final String accountCurrency = "SEK";
        final String illegalArgumentString = "Illegal-argument-junit-test-check";

        doThrow(new IllegalArgumentException(illegalArgumentString)) //
                .when(accountManagementEndpoint) //
                .create(eq(accountName), anyString());

        AccountCreateInput input = new AccountCreateInput();
        input.setAccountId(accountName);
        input.setCurrency(accountCurrency);
        final String accountCreateAsString = gsonConverter.toJson(input);

        HttpPost httpCall = new HttpPost("http://localhost:9090/rest/v1/account");
        httpCall.setEntity(new StringEntity(accountCreateAsString));
        httpCall.setHeader("Accept", "application/json");
        httpCall.setHeader("Content-type", "application/json");

        // verify http call is ok
        try (CloseableHttpResponse response = httpClient.execute(httpCall)) {
            final StatusLine responseStatus = response.getStatusLine();
            Assert.assertNotNull(responseStatus);
            Assert.assertEquals("Account created shall respond with http-bad-request", 400, responseStatus.getStatusCode());

            final InputStreamReader jsonStreamReader = new InputStreamReader(response.getEntity().getContent());
            final ErrorDescription errorDescription = gsonConverter.fromJson(jsonStreamReader, ErrorDescription.class);
            Assert.assertTrue(errorDescription.getDescription().contains(illegalArgumentString));
        }
    }

    @Test
    public void verify_broken_json_gives_400() throws IOException, AccountAlreadyExistsException, ApplicationDataAccessError {
        final String accountName = "TEST_ACCT_0001";
        final String illegalArgumentString = "Illegal-argument-junit-test-check";

        doThrow(new IllegalArgumentException(illegalArgumentString)) //
                .when(accountManagementEndpoint) //
                .create(eq(accountName), anyString());

        HttpPost httpCall = new HttpPost("http://localhost:9090/rest/v1/account");
        httpCall.setEntity(new StringEntity("ansdjsadjnj"));
        httpCall.setHeader("Accept", "application/json");
        httpCall.setHeader("Content-type", "application/json");

        // verify http call is ok
        try (CloseableHttpResponse response = httpClient.execute(httpCall)) {
            final StatusLine responseStatus = response.getStatusLine();
            Assert.assertNotNull(responseStatus);
            Assert.assertEquals("Account created shall respond with http-bad-request", 418, responseStatus.getStatusCode());
        }

        verify(accountManagementEndpoint, times(0)).create(anyString(), anyString());
    }


    @Test
    public void verify_account_creation_call_fails_on_duplicate() throws IOException, AccountAlreadyExistsException, ApplicationDataAccessError {
        final String accountName = "TEST_ACCT_0001_DUPLICATE";
        final String accountCurrency = "SEK";

        doThrow(new AccountAlreadyExistsException(accountName)) //
                .when(accountManagementEndpoint) //
                .create(eq(accountName), anyString());

        AccountCreateInput input = new AccountCreateInput();
        input.setAccountId(accountName);
        input.setCurrency(accountCurrency);
        final String accountCreateAsString = gsonConverter.toJson(input);

        HttpPost httpCall = new HttpPost("http://localhost:9090/rest/v1/account");
        httpCall.setEntity(new StringEntity(accountCreateAsString));
        httpCall.setHeader("Accept", "application/json");
        httpCall.setHeader("Content-type", "application/json");

        // verify http call is ok
        try (CloseableHttpResponse response = httpClient.execute(httpCall)) {
            final StatusLine responseStatus = response.getStatusLine();
            Assert.assertNotNull(responseStatus);
            Assert.assertEquals("Account created shall respond with http-conflict", 409, responseStatus.getStatusCode());
        }

        // make sure parameters are passed
        verify(accountManagementEndpoint, times(1)).create(eq(accountName), eq(accountCurrency));
    }

    @Test
    public void verify_can_get_existing_account() throws IOException, AccountNotFoundException, ApplicationDataAccessError {
        final String accountName = "TEST_ACCT_0002";
        final String accountCurrency = "SEK";
        final String accountAmount = "101.10";

        final AccountDescription value = new AccountDescription();
        value.setAccountId(accountName);
        value.setBalance(accountAmount);
        value.setCurrency(accountCurrency);
        when(accountManagementEndpoint.get(eq(accountName))).thenReturn(value);

        HttpGet httpCall = new HttpGet("http://localhost:9090/rest/v1/account/" + accountName);
        httpCall.setHeader("Accept", "application/json");
        httpCall.setHeader("Content-type", "application/json");

        // verify http call is ok
        try (CloseableHttpResponse response = httpClient.execute(httpCall)) {
            final StatusLine responseStatus = response.getStatusLine();
            Assert.assertNotNull(responseStatus);
            Assert.assertEquals("Account should exist", 200, responseStatus.getStatusCode());
            Assert.assertTrue(response.containsHeader("Content-type"));
            Assert.assertEquals("application/json", response.getFirstHeader("Content-type").getValue());

            final InputStreamReader jsonStreamReader = new InputStreamReader(response.getEntity().getContent());
            final AccountDescription accountDescription = gsonConverter.fromJson(jsonStreamReader, AccountDescription.class);

            Assert.assertEquals(accountName, accountDescription.getAccountId());
            Assert.assertEquals(accountCurrency, accountDescription.getCurrency());
            Assert.assertEquals(accountAmount, accountDescription.getBalance());
        }

        // make sure parameters are passed
        verify(accountManagementEndpoint, times(1)).get(eq(accountName));
    }

    @Test
    public void verify_can_not_get_non_existing_account() throws IOException, AccountNotFoundException, ApplicationDataAccessError {
        final String accountName = "TEST_ACCT_0002";

        when(accountManagementEndpoint.get(eq(accountName))).thenThrow(new AccountNotFoundException(accountName));

        HttpGet httpCall = new HttpGet("http://localhost:9090/rest/v1/account/" + accountName);
        httpCall.setHeader("Accept", "application/json");
        httpCall.setHeader("Content-type", "application/json");

        // verify http call is ok
        try (CloseableHttpResponse response = httpClient.execute(httpCall)) {
            final StatusLine responseStatus = response.getStatusLine();
            Assert.assertNotNull(responseStatus);
            Assert.assertEquals("Account does not exist", 404, responseStatus.getStatusCode());

            final InputStreamReader jsonStreamReader = new InputStreamReader(response.getEntity().getContent());
            final ErrorDescription errorDescription = gsonConverter.fromJson(jsonStreamReader, ErrorDescription.class);
            Assert.assertTrue(errorDescription.getDescription().contains(accountName));
        }

        // make sure parameters are passed
        verify(accountManagementEndpoint, times(1)).get(eq(accountName));
    }

    @Test
    public void verify_can_create_money_transfer() throws IOException, ApplicationException {
        final String srcAccountName = "TEST_ACCT_0001_SRC_0";
        final String dstAccountName = "TEST_ACCT_0001_DST_0";
        final String transferCurrency = "SEK";
        final String transferAmount = "765.12";
        final String clientTxId = UUID.randomUUID().toString();

        MoneyTransferInput input = new MoneyTransferInput();
        input.setDestinationAccount(dstAccountName);
        input.setCurrency(transferCurrency);
        input.setAmount(transferAmount);
        input.setId(clientTxId);
        final String transferCreateAsAString = gsonConverter.toJson(input);

        HttpPost httpCall = new HttpPost("http://localhost:9090/rest/v1/account/" + srcAccountName + "/transfer");
        httpCall.setEntity(new StringEntity(transferCreateAsAString));
        httpCall.setHeader("Accept", "application/json");
        httpCall.setHeader("Content-type", "application/json");

        // verify http call is ok
        try (CloseableHttpResponse response = httpClient.execute(httpCall)) {
            final StatusLine responseStatus = response.getStatusLine();
            Assert.assertNotNull(responseStatus);
            Assert.assertEquals("Transfer created shall respond with http-created", 201, responseStatus.getStatusCode());
        }

        // make sure parameters are passed
        verify(transferManagementEndpoint, times(1)) //
                .transfer(eq(srcAccountName), eq(dstAccountName), eq(transferAmount), eq(transferCurrency), eq(clientTxId));
    }

    @Test
    public void verify_retransfer_return_error() throws IOException, ApplicationException {
        final String srcAccountName = "TEST_ACCT_0001_SRC_0";
        final String dstAccountName = "TEST_ACCT_0001_DST_0";
        final String transferCurrency = "SEK";
        final String transferAmount = "765.12";
        final String clientTxId = UUID.randomUUID().toString();

        doThrow(new TransferAlreadyProcessedException(srcAccountName, clientTxId))
                .when(transferManagementEndpoint).transfer(eq(srcAccountName), eq(dstAccountName), anyString(), anyString(), anyString());

        MoneyTransferInput input = new MoneyTransferInput();
        input.setDestinationAccount(dstAccountName);
        input.setCurrency(transferCurrency);
        input.setAmount(transferAmount);
        input.setId(clientTxId);
        final String transferCreateAsAString = gsonConverter.toJson(input);

        HttpPost httpCall = new HttpPost("http://localhost:9090/rest/v1/account/" + srcAccountName + "/transfer");
        httpCall.setEntity(new StringEntity(transferCreateAsAString));
        httpCall.setHeader("Accept", "application/json");
        httpCall.setHeader("Content-type", "application/json");

        // verify http call is ok
        try (CloseableHttpResponse response = httpClient.execute(httpCall)) {
            final StatusLine responseStatus = response.getStatusLine();
            Assert.assertNotNull(responseStatus);
            Assert.assertEquals("Should have an error", 409, responseStatus.getStatusCode());

            final InputStreamReader jsonStreamReader = new InputStreamReader(response.getEntity().getContent());
            final ErrorDescription errorDescription = gsonConverter.fromJson(jsonStreamReader, ErrorDescription.class);
            Assert.assertTrue(errorDescription.getDescription().contains(srcAccountName));
        }
    }

    @Test
    public void verify_can_not_transfer_non_existing_account() throws IOException, ApplicationException {
        final String srcAccountName = "TEST_ACCT_0001_SRC_NF";
        final String dstAccountName = "TEST_ACCT_0001_DST_NF";
        final String transferCurrency = "SEK";
        final String transferAmount = "765.12";
        final String clientTxId = UUID.randomUUID().toString();

        doThrow(new AccountNotFoundException(srcAccountName))
                .when(transferManagementEndpoint).transfer(eq(srcAccountName), eq(dstAccountName), anyString(), anyString(), anyString());

        MoneyTransferInput input = new MoneyTransferInput();
        input.setDestinationAccount(dstAccountName);
        input.setCurrency(transferCurrency);
        input.setAmount(transferAmount);
        input.setId(clientTxId);
        final String transferCreateAsAString = gsonConverter.toJson(input);

        HttpPost httpCall = new HttpPost("http://localhost:9090/rest/v1/account/" + srcAccountName + "/transfer");
        httpCall.setEntity(new StringEntity(transferCreateAsAString));
        httpCall.setHeader("Accept", "application/json");
        httpCall.setHeader("Content-type", "application/json");

        // verify http call is ok
        try (CloseableHttpResponse response = httpClient.execute(httpCall)) {
            final StatusLine responseStatus = response.getStatusLine();
            Assert.assertNotNull(responseStatus);
            Assert.assertEquals("Account not found", 404, responseStatus.getStatusCode());
        }

        // make sure parameters are passed
        verify(transferManagementEndpoint, times(1)) //
                .transfer(eq(srcAccountName), eq(dstAccountName), eq(transferAmount), eq(transferCurrency), eq(clientTxId));
    }

    @Test
    public void verify_can_not_transfer_no_sufficent_balance() throws IOException, ApplicationException {
        final String srcAccountName = "TEST_ACCT_0001_SRC_NF";
        final String dstAccountName = "TEST_ACCT_0001_DST_NF";
        final String transferCurrency = "SEK";
        final String transferAmount = "765.12";
        final String clientTxId = UUID.randomUUID().toString();

        doThrow(new NoSufficientFundsException(srcAccountName, transferAmount, "ACCT_CURRENCY"))
                .when(transferManagementEndpoint).transfer(eq(srcAccountName), eq(dstAccountName), anyString(), anyString(), anyString());

        MoneyTransferInput input = new MoneyTransferInput();
        input.setDestinationAccount(dstAccountName);
        input.setCurrency(transferCurrency);
        input.setAmount(transferAmount);
        input.setId(clientTxId);
        final String transferCreateAsAString = gsonConverter.toJson(input);

        HttpPost httpCall = new HttpPost("http://localhost:9090/rest/v1/account/" + srcAccountName + "/transfer");
        httpCall.setEntity(new StringEntity(transferCreateAsAString));
        httpCall.setHeader("Accept", "application/json");
        httpCall.setHeader("Content-type", "application/json");

        // verify http call is ok
        try (CloseableHttpResponse response = httpClient.execute(httpCall)) {
            final StatusLine responseStatus = response.getStatusLine();
            Assert.assertNotNull(responseStatus);
            Assert.assertEquals("Account not found", 400, responseStatus.getStatusCode());
        }

        // make sure parameters are passed
        verify(transferManagementEndpoint, times(1)) //
                .transfer(eq(srcAccountName), eq(dstAccountName), eq(transferAmount), eq(transferCurrency), eq(clientTxId));
    }

    @Test
    public void verify_can_get_list_of_transfers_for_account() throws IOException, ApplicationException {
        final String srcAccountName = "EXISTING_ACCOUNT";

        final TransferDescription transferDescription = new TransferDescription();
        transferDescription.setAccountSourceId(srcAccountName);
        transferDescription.setAmount("125");
        transferDescription.setCurrency("EUR");
        transferDescription.setAccountDestinationId("DESTINATION_ACCOUNT");
        transferDescription.setWhen(777);
        when(transferQueryEndpoint.getTransfersForAccount(eq(srcAccountName)))
                .thenReturn(Arrays.asList(transferDescription));

        HttpGet httpCall = new HttpGet("http://localhost:9090/rest/v1/account/" + srcAccountName + "/transfer");
        httpCall.setHeader("Accept", "application/json");
        httpCall.setHeader("Content-type", "application/json");

        // verify http call is ok
        try (CloseableHttpResponse response = httpClient.execute(httpCall)) {
            final StatusLine responseStatus = response.getStatusLine();
            Assert.assertNotNull(responseStatus);
            Assert.assertEquals("Account not found", 200, responseStatus.getStatusCode());

            final InputStreamReader jsonStreamReader = new InputStreamReader(response.getEntity().getContent());
            final TransferDescriptionOutput[] transfers = gsonConverter.fromJson(jsonStreamReader, TransferDescriptionOutput[].class);

            Assert.assertNotNull(transfers);
            Assert.assertEquals(1, transfers.length);

            Assert.assertNotNull(transfers[0]);
            Assert.assertEquals(srcAccountName, transfers[0].getAccountSourceId());
        }

        // make sure parameters are passed
        verify(transferQueryEndpoint, times(1)) //
                .getTransfersForAccount(eq(srcAccountName));
    }

    @Test
    public void verify_non_existing_account_returns_404() throws IOException, ApplicationException {
        final String srcAccountName = "NON_EXISTING_ACCOUNT";

        when(transferQueryEndpoint.getTransfersForAccount(eq(srcAccountName)))
                .thenThrow(new AccountNotFoundException(srcAccountName));

        HttpGet httpCall = new HttpGet("http://localhost:9090/rest/v1/account/" + srcAccountName + "/transfer");
        httpCall.setHeader("Accept", "application/json");
        httpCall.setHeader("Content-type", "application/json");

        // verify http call is ok
        try (CloseableHttpResponse response = httpClient.execute(httpCall)) {
            final StatusLine responseStatus = response.getStatusLine();
            Assert.assertNotNull(responseStatus);
            Assert.assertEquals("Account not found", 404, responseStatus.getStatusCode());

            final InputStreamReader jsonStreamReader = new InputStreamReader(response.getEntity().getContent());
            final ErrorDescription errorDescription = gsonConverter.fromJson(jsonStreamReader, ErrorDescription.class);
            Assert.assertTrue(errorDescription.getDescription().contains(srcAccountName));
        }

        // make sure parameters are passed
        verify(transferQueryEndpoint, times(1)) //
                .getTransfersForAccount(eq(srcAccountName));
    }
}
