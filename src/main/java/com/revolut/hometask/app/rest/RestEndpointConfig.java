package com.revolut.hometask.app.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Spark;

@Singleton
public class RestEndpointConfig {

    private final static Logger LOG = LoggerFactory.getLogger(RestEndpointConfig.class.getCanonicalName());

    private final AccountManagementEndpoint accountManagementEndpoint;

    private final TransferManagementEndpoint transferManagementEndpoint;

    private final TransferQueryEndpoint transferQueryEndpoint;

    private final Gson gson;

    @Inject
    public RestEndpointConfig(AccountManagementEndpoint accountManagementEndpoint,
                              TransferManagementEndpoint transferManagementEndpoint,
                              TransferQueryEndpoint transferQueryEndpoint) {
        this.accountManagementEndpoint = accountManagementEndpoint;
        this.transferManagementEndpoint = transferManagementEndpoint;
        this.transferQueryEndpoint = transferQueryEndpoint;

        // TODO inject?
        final GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();
    }

    public void run(int portNumber) {
        LOG.info("Starting on port {}", portNumber);
        Spark.port(portNumber);

        // account management REST calls
        Spark.path(
                "/rest/v1/",
                RoutesV1.coreRestApiV1Routes(accountManagementEndpoint, transferManagementEndpoint,
                        transferQueryEndpoint, gson)
        );

        // general
        Spark.get("/health", (request, response) -> {
            return "I'm ok";
        });


    }

}
