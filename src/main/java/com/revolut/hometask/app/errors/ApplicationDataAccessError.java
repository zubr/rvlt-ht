package com.revolut.hometask.app.errors;

public class ApplicationDataAccessError extends ApplicationException {
    public ApplicationDataAccessError() {
    }

    public ApplicationDataAccessError(String message) {
        super(message);
    }

    public ApplicationDataAccessError(String message, Throwable cause) {
        super(message, cause);
    }

    public ApplicationDataAccessError(Throwable cause) {
        super(cause);
    }

    public ApplicationDataAccessError(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
