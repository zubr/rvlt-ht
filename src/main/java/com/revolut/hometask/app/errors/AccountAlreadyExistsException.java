package com.revolut.hometask.app.errors;

public class AccountAlreadyExistsException extends ApplicationException {

    private final String accountId;

    public AccountAlreadyExistsException(String accountId) {
        super("Account already exists. AccountId=" + accountId);
        this.accountId = accountId;
    }

    public String getAccountId() {
        return accountId;
    }
}
