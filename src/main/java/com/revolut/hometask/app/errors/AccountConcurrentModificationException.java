package com.revolut.hometask.app.errors;

public class AccountConcurrentModificationException extends ApplicationException {
    public AccountConcurrentModificationException() {
    }

    public AccountConcurrentModificationException(String message) {
        super(message);
    }

    public AccountConcurrentModificationException(String message, Throwable cause) {
        super(message, cause);
    }

    public AccountConcurrentModificationException(Throwable cause) {
        super(cause);
    }

    public AccountConcurrentModificationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
