package com.revolut.hometask.app.rest.endpoint;

import com.revolut.hometask.app.business.AccountManagementService;
import com.revolut.hometask.app.business.Transfer;
import com.revolut.hometask.app.business.TransferQueryService;
import com.revolut.hometask.app.errors.AccountNotFoundException;
import com.revolut.hometask.app.errors.ApplicationDataAccessError;
import com.revolut.hometask.app.rest.TransferDescription;
import com.revolut.hometask.app.rest.impl.TransferQueryEndpointImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TransferQueryEndpointImplTests {

    private final static String TEST_ACCT_NAME = "query.transfers.junit";

    @InjectMocks
    private TransferQueryEndpointImpl service;

    @Mock
    private TransferQueryService queryService;

    @Mock
    private AccountManagementService accountManagementService;

    @Test
    public void simple_check_parameters_are_passed() throws ApplicationDataAccessError, AccountNotFoundException {

        final Transfer transfer = new Transfer();
        transfer.setAccountSourceId(TEST_ACCT_NAME);
        transfer.setAmount("123.45");
        transfer.setCurrency("SEK");
        transfer.setWhen(678);
        transfer.setAccountDestinationId("DEST_ACCT" + System.nanoTime());

        when(queryService.getTransfersForAccount(eq(TEST_ACCT_NAME)))
                .thenReturn(Arrays.asList(transfer));

        final List<TransferDescription> transfersForAccount = service.getTransfersForAccount(TEST_ACCT_NAME);

        Assert.assertNotNull(transfersForAccount);
        Assert.assertEquals(1, transfersForAccount.size());
        Assert.assertNotNull(transfersForAccount.get(0));
        Assert.assertEquals(TEST_ACCT_NAME, transfersForAccount.get(0).getAccountSourceId());
        Assert.assertEquals(transfer.getAccountDestinationId(), transfersForAccount.get(0).getAccountDestinationId());
        Assert.assertEquals(transfer.getCurrency(), transfersForAccount.get(0).getCurrency());
        Assert.assertEquals(transfer.getWhen(), transfersForAccount.get(0).getWhen());
        Assert.assertEquals(transfer.getAmount(), transfersForAccount.get(0).getAmount());
    }

    @Test(expected = AccountNotFoundException.class)
    public void account_not_found_case_expect_exception() throws ApplicationDataAccessError, AccountNotFoundException {
        when(accountManagementService.get(eq(TEST_ACCT_NAME)))
                .thenThrow(new AccountNotFoundException(TEST_ACCT_NAME));

        service.getTransfersForAccount(TEST_ACCT_NAME);
    }

    @Test(expected = ApplicationDataAccessError.class)
    public void error_accessing_data_storage_get_account_fail() throws ApplicationDataAccessError, AccountNotFoundException {
        when(accountManagementService.get(eq(TEST_ACCT_NAME)))
                .thenThrow(new ApplicationDataAccessError(TEST_ACCT_NAME));

        service.getTransfersForAccount(TEST_ACCT_NAME);
    }

    @Test(expected = ApplicationDataAccessError.class)
    public void error_accessing_data_storage_get_transfer_fail() throws ApplicationDataAccessError, AccountNotFoundException {
        when(queryService.getTransfersForAccount(eq(TEST_ACCT_NAME)))
                .thenThrow(new ApplicationDataAccessError(TEST_ACCT_NAME));

        service.getTransfersForAccount(TEST_ACCT_NAME);
    }

    @Test
    public void simple_empty_array() throws ApplicationDataAccessError, AccountNotFoundException {
        when(queryService.getTransfersForAccount(eq(TEST_ACCT_NAME)))
                .thenReturn(new ArrayList<>());

        final List<TransferDescription> transfersForAccount = service.getTransfersForAccount(TEST_ACCT_NAME);

        Assert.assertNotNull(transfersForAccount);
        Assert.assertEquals(0, transfersForAccount.size());
    }

}
