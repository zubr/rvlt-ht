package com.revolut.hometask.app.errors;

public class NoSufficientFundsException extends ApplicationException {

    private final String srcAccountId;

    private final String amount;

    private final String amountCurrency;

    public NoSufficientFundsException(String srcAccountId, String amount, String amountCurrency) {
        super("Insufficient funds exception. From " + srcAccountId + " " + amount + amountCurrency);
        this.srcAccountId = srcAccountId;
        this.amount = amount;
        this.amountCurrency = amountCurrency;
    }

    public String getSrcAccountId() {
        return srcAccountId;
    }

    public String getAmount() {
        return amount;
    }

    public String getAmountCurrency() {
        return amountCurrency;
    }
}
