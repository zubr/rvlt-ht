package com.revolut.hometask.app.business.db;

import com.revolut.hometask.app.business.Account;
import com.revolut.hometask.app.business.ClockService;
import com.revolut.hometask.app.errors.*;
import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.*;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicInteger;

public class H2AccountServiceTest {

    private static TestDatabaseConnectionProvider dbConnectionProvider;
    private static JdbcConnectionPool connectionPool;
    private static AtomicInteger counter = new AtomicInteger(0);
    
    private final String accountId = "h2-junit-account-id";
    private final String accountTransferId = accountId + ".transfer";
    private final String currencyCodeEUR = "eur";
    private H2AccountService accountService;
    private H2TransferMoneyService transferMoneyService;

    @BeforeClass
    public static void prepareDb() {
        dbConnectionProvider = new TestDatabaseConnectionProvider();
        connectionPool = dbConnectionProvider.getConnectionPool();
    }

    @AfterClass
    public static void clearDb() throws SQLException {
        connectionPool.getConnection().prepareStatement("drop all objects").execute();
    }

    private String generateAccountName() {
        return accountId + "." + counter.incrementAndGet();
    }

    @Before
    public void prepare() {
        accountService = new H2AccountService(connectionPool);
        transferMoneyService = new H2TransferMoneyService(connectionPool, new ClockService() {
            @Override
            public long now() {
                return System.currentTimeMillis();
            }
        });
    }

    @Test
    public void createAccountAndGetDetails() throws AccountAlreadyExistsException, AccountNotFoundException, ApplicationDataAccessError {
        accountService.create(accountId, currencyCodeEUR);

        final Account account = accountService.get(accountId);
        Assert.assertEquals(new BigDecimal("0.00"), account.getBalance());
        Assert.assertEquals(currencyCodeEUR, account.getCurrency());
        Assert.assertEquals(accountId, account.getAccountId());
    }

    @Test
    public void createAccountAndUpdateAndGetDetails() throws ApplicationException {
        final String testAccountId = generateAccountName();

        accountService.create(testAccountId, currencyCodeEUR);
        Account account = accountService.get(this.accountId);
        accountService.update(testAccountId, account.getVersion(), new BigDecimal("321.45"));

        account = accountService.get(testAccountId);
        Assert.assertEquals(new BigDecimal("321.45"), account.getBalance());
        Assert.assertEquals(currencyCodeEUR, account.getCurrency());
        Assert.assertEquals(testAccountId, account.getAccountId());
    }

    @Test
    public void transferMoney_single_transaction() throws ApplicationException {
        final String testAccountId1 = generateAccountName();
        final String testAccountId2 = generateAccountName();

        accountService.create(testAccountId1, currencyCodeEUR);
        Account account = accountService.get(testAccountId1);
        accountService.update(testAccountId1, account.getVersion(), new BigDecimal("321.45"));

        accountService.create(testAccountId2, currencyCodeEUR);

        final BigDecimal transferAmount = new BigDecimal("0.45");
        transferMoneyService.transfer(accountService.get(testAccountId1), transferAmount.negate(),
                accountService.get(testAccountId2), transferAmount,
                transferAmount, "EUR", accountTransferId + System.currentTimeMillis());

        account = accountService.get(testAccountId1);
        Assert.assertEquals(new BigDecimal("321.00"), account.getBalance());
        Assert.assertEquals(currencyCodeEUR, account.getCurrency());
        Assert.assertEquals(testAccountId1, account.getAccountId());

        account = accountService.get(testAccountId2);
        Assert.assertEquals(new BigDecimal("0.45"), account.getBalance());
        Assert.assertEquals(currencyCodeEUR, account.getCurrency());
        Assert.assertEquals(testAccountId2, account.getAccountId());
    }

    @Test(expected = TransferAlreadyProcessedException.class)
    public void transferMoney_repeat_single_transaction() throws ApplicationException {
        final String testAccountId1 = generateAccountName();
        final String testAccountId2 = generateAccountName();

        accountService.create(testAccountId1, currencyCodeEUR);
        Account account = accountService.get(testAccountId1);
        accountService.update(testAccountId1, account.getVersion(), new BigDecimal("321.45"));

        accountService.create(testAccountId2, currencyCodeEUR);

        final BigDecimal transferAmount = new BigDecimal("0.45");

        final String clientTransferId = accountTransferId + System.currentTimeMillis();
        transferMoneyService.transfer(accountService.get(testAccountId1), transferAmount,
                accountService.get(testAccountId2), transferAmount,
                transferAmount, "EUR", clientTransferId);

        transferMoneyService.transfer(accountService.get(testAccountId1), transferAmount,
                accountService.get(testAccountId2), transferAmount,
                transferAmount, "EUR", clientTransferId);
    }

    @Test
    public void transferMoney_several_transaction() throws ApplicationException {
        final String testAccountId1 = generateAccountName();
        final String testAccountId2 = generateAccountName();

        accountService.create(testAccountId1, currencyCodeEUR);
        Account account = accountService.get(this.accountId);
        accountService.update(testAccountId1, account.getVersion(), new BigDecimal("321.45"));

        accountService.create(testAccountId2, currencyCodeEUR);

        BigDecimal transferAmount = new BigDecimal("0.45");
        transferMoneyService.transfer(accountService.get(testAccountId1), transferAmount.negate(),
                accountService.get(testAccountId2), transferAmount, transferAmount, "EUR",
                accountTransferId + System.currentTimeMillis());

        transferAmount = new BigDecimal("10.00");
        transferMoneyService.transfer(accountService.get(testAccountId1), transferAmount.negate(),
                accountService.get(testAccountId2), transferAmount, transferAmount, "EUR",
                accountTransferId + System.currentTimeMillis());

        transferAmount = new BigDecimal("7");
        transferMoneyService.transfer(accountService.get(testAccountId1), transferAmount.negate(),
                accountService.get(testAccountId2), transferAmount, transferAmount, "EUR",
                accountTransferId + System.currentTimeMillis());

        account = accountService.get(testAccountId1);
        Assert.assertEquals(new BigDecimal("304.00"), account.getBalance());
        Assert.assertEquals(currencyCodeEUR, account.getCurrency());
        Assert.assertEquals(testAccountId1, account.getAccountId());

        account = accountService.get(testAccountId2);
        Assert.assertEquals(new BigDecimal("17.45"), account.getBalance());
        Assert.assertEquals(currencyCodeEUR, account.getCurrency());
        Assert.assertEquals(testAccountId2, account.getAccountId());
    }

    @Test(expected = AccountAlreadyExistsException.class)
    public void createDuplicateAccount() throws AccountAlreadyExistsException, ApplicationDataAccessError {
        final String testAccountId = generateAccountName();

        accountService.create(testAccountId, currencyCodeEUR);
        accountService.create(testAccountId, currencyCodeEUR);
    }

}
