package com.revolut.hometask.app.rest.dto;

public class ErrorDescription {

    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
