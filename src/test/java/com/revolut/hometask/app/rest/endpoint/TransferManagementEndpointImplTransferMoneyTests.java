package com.revolut.hometask.app.rest.endpoint;

import com.revolut.hometask.app.business.*;
import com.revolut.hometask.app.errors.ApplicationException;
import com.revolut.hometask.app.rest.impl.TransferManagementEndpointImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class TransferManagementEndpointImplTransferMoneyTests {

    private final String srcAccountId = "junit-account-src";

    private final String dstAccountId = "junit-account-dst";

    private final String currencyCodeEUR = "EUR";

    @InjectMocks
    private TransferManagementEndpointImpl service;

    @Mock
    private AccountManagementService accountManagementService;

    @Mock
    private ExchangeRateService exchangeRateService;

    @Mock
    private MoneyTransferService moneyTransferService;

    @Mock
    private ClockService clockService;

    @Test
    public void transferAllOk() throws ApplicationException {
        when(exchangeRateService.isCurrencyValid(eq(currencyCodeEUR)))
                .thenReturn(Boolean.TRUE);

        when(exchangeRateService.convertAmountBetweenCurrencies(any(), any(), any(), anyLong()))
                .thenAnswer(invocationOnMock -> {
                    return invocationOnMock.getArgument(2);
                });

        final Account srcAccount = new Account();
        when(accountManagementService.get(eq(srcAccountId)))
                .thenReturn(srcAccount);

        final Account dstAccount = new Account();
        when(accountManagementService.get(eq(dstAccountId)))
                .thenReturn(dstAccount);

        final String transferAmount = "1234.56";
        final String clientGeneratedTransactionId = UUID.randomUUID().toString();
        service.transfer(srcAccountId, dstAccountId, transferAmount, currencyCodeEUR, clientGeneratedTransactionId);

        verify(moneyTransferService, times(1)).transfer(
                eq(srcAccount), eq(new BigDecimal(transferAmount).negate()),
                eq(dstAccount), eq(new BigDecimal(transferAmount)),
                eq(new BigDecimal(transferAmount)), eq(currencyCodeEUR),
                eq(clientGeneratedTransactionId)
        );
    }

    @Test
    public void verify_delta_amounts_passed_correctly() throws ApplicationException {
        when(exchangeRateService.isCurrencyValid(eq(currencyCodeEUR)))
                .thenReturn(Boolean.TRUE);

        final Account srcAccount = new Account();
        srcAccount.setCurrency("junitSrcCurrency");
        when(accountManagementService.get(eq(srcAccountId)))
                .thenReturn(srcAccount);
        BigDecimal transferFrom = new BigDecimal("4890.11");

        when(exchangeRateService.convertAmountBetweenCurrencies(eq(currencyCodeEUR), eq(srcAccount.getCurrency()), any(), anyLong()))
                .thenAnswer(invocationOnMock -> {
                    return transferFrom;
                });

        final Account dstAccount = new Account();
        dstAccount.setCurrency("junitDstCurrency");
        when(accountManagementService.get(eq(dstAccountId)))
                .thenReturn(dstAccount);
        BigDecimal transferTo = new BigDecimal("9890.11");

        when(exchangeRateService.convertAmountBetweenCurrencies(eq(currencyCodeEUR), eq(dstAccount.getCurrency()), any(), anyLong()))
                .thenAnswer(invocationOnMock -> {
                    return transferTo;
                });

        final String transferAmount = "1234.56";
        final String clientGeneratedTransactionId = UUID.randomUUID().toString();
        service.transfer(srcAccountId, dstAccountId, transferAmount, currencyCodeEUR, clientGeneratedTransactionId);

        verify(moneyTransferService, times(1)).transfer(
                eq(srcAccount), eq(transferFrom.negate()),
                eq(dstAccount), eq(transferTo),
                eq(new BigDecimal(transferAmount)), eq(currencyCodeEUR),
                eq(clientGeneratedTransactionId)
        );
    }

}
