package com.revolut.hometask.app.rest.impl;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.revolut.hometask.app.business.AccountManagementService;
import com.revolut.hometask.app.business.Transfer;
import com.revolut.hometask.app.business.TransferQueryService;
import com.revolut.hometask.app.errors.AccountNotFoundException;
import com.revolut.hometask.app.errors.ApplicationDataAccessError;
import com.revolut.hometask.app.rest.TransferDescription;
import com.revolut.hometask.app.rest.TransferQueryEndpoint;

import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class TransferQueryEndpointImpl implements TransferQueryEndpoint {

    private final AccountManagementService accountManagementService;

    private final TransferQueryService transferQueryService;

    @Inject
    public TransferQueryEndpointImpl(AccountManagementService accountManagementService, TransferQueryService transferQueryService) {
        this.accountManagementService = accountManagementService;
        this.transferQueryService = transferQueryService;
    }

    @Override
    public List<TransferDescription> getTransfersForAccount(String account) throws ApplicationDataAccessError, AccountNotFoundException {
        accountManagementService.get(account);

        return transferQueryService.getTransfersForAccount(account)
                .stream().map(TransferQueryEndpointImpl::convert).collect(Collectors.toList());
    }

    @Override
    public List<TransferDescription> getTransfersIntoAccount(String account) throws ApplicationDataAccessError, AccountNotFoundException {
        accountManagementService.get(account);

        return transferQueryService.getTransfersIntoAccount(account)
                .stream().map(TransferQueryEndpointImpl::convert).collect(Collectors.toList());
    }

    @Override
    public List<TransferDescription> getTransfersPaymentsFromAccount(String account) throws AccountNotFoundException, ApplicationDataAccessError {
        accountManagementService.get(account);

        return transferQueryService.getTransfersPaymentsFromAccount(account)
                .stream().map(TransferQueryEndpointImpl::convert).collect(Collectors.toList());
    }

    protected static TransferDescription convert(Transfer transfer) {
        final TransferDescription transferDescription = new TransferDescription();
        transferDescription.setAccountSourceId(transfer.getAccountSourceId());
        transferDescription.setAccountDestinationId(transfer.getAccountDestinationId());
        transferDescription.setAmount(transfer.getAmount());
        transferDescription.setCurrency(transfer.getCurrency());
        transferDescription.setWhen(transfer.getWhen());
        return transferDescription;
    }
}
