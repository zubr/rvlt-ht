package com.revolut.hometask.app.business.memory;

import com.revolut.hometask.app.business.Account;
import com.revolut.hometask.app.business.ClockService;
import com.revolut.hometask.app.errors.ApplicationException;
import com.revolut.hometask.app.errors.NoSufficientFundsException;
import com.revolut.hometask.app.errors.TransferAlreadyProcessedException;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public abstract class ForFunlementationBaseScenarios {

    private final String eurCurrencyCode = "EUR";

    protected InMemoryBaseHandler service;

    protected ClockService clockService;

    @Test
    public void checkAllOkTx() throws ApplicationException {
        /// create ATM
        service.create("BANK_ATM", eurCurrencyCode, new BigDecimal("10000.00"));
        {
            final List bankAtmTxs = service.getTransfersForAccount("BANK_ATM");
            Assert.assertNotNull(bankAtmTxs);
            Assert.assertEquals(1, bankAtmTxs.size());
        }

        /// create Rick account
        service.create("Rick", eurCurrencyCode);
        {
            final List rickTxs = service.getTransfersForAccount("Rick");
            Assert.assertNotNull(rickTxs);
            Assert.assertEquals(0, rickTxs.size());
        }

        service.transfer(service.get("BANK_ATM"), new BigDecimal("9000.00").negate(),
                service.get("Rick"), new BigDecimal("9000.00"),
                new BigDecimal("9000.00"), "EUR", UUID.randomUUID().toString());
        /// lets see if money transferred?
        {
            final Account bankAtm = service.get("BANK_ATM");
            Assert.assertEquals(new BigDecimal("1000.00"), bankAtm.getBalance());
        }
        {
            final Account bankAtm = service.get("Rick");
            Assert.assertEquals(new BigDecimal("9000.00"), bankAtm.getBalance());
        }
    }

    @Test
    public void ensure_account_update_correct() throws ApplicationException {
        /// create ATM
        service.create("BANK_ATM", eurCurrencyCode, new BigDecimal("10000.00"));
        final Account banAccount = service.get("BANK_ATM");

        service.update("BANK_ATM", banAccount.getVersion(), new BigDecimal("777.0"));

        {
            final Account bankAtm = service.get("BANK_ATM");
            Assert.assertEquals(new BigDecimal("777.00"), bankAtm.getBalance());
        }
    }

    @Test
    public void transfer_several_times() throws ApplicationException {
        /// create ATM
        service.create("BANK_ATM", eurCurrencyCode, new BigDecimal("10000.00"));
        /// create Rick account
        service.create("Rick", eurCurrencyCode);

        /// different transactions
        service.transfer(service.get("BANK_ATM"), new BigDecimal("1000.00").negate(),
                service.get("Rick"), new BigDecimal("1000.00"),
                new BigDecimal("1000.00"), "EUR", UUID.randomUUID().toString());

        /// different transactions
        service.transfer(service.get("BANK_ATM"), new BigDecimal("750.00").negate(),
                service.get("Rick"), new BigDecimal("750.00"),
                new BigDecimal("750.00"), "EUR", UUID.randomUUID().toString());

        /// lets see if money transferred?
        {
            final Account bankAtm = service.get("Rick");
            Assert.assertEquals(new BigDecimal("1750.00"), bankAtm.getBalance());

            final List rickTxs = service.getTransfersForAccount("Rick");
            Assert.assertNotNull(rickTxs);
            Assert.assertEquals(2, rickTxs.size());
        }
        {
            final Account bankAtm = service.get("BANK_ATM");
            Assert.assertEquals(new BigDecimal("8250.00"), bankAtm.getBalance());

            final List bankAtmTxs = service.getTransfersForAccount("BANK_ATM");
            Assert.assertNotNull(bankAtmTxs);
            Assert.assertEquals(3, bankAtmTxs.size());
        }
    }

    @Test(expected = NoSufficientFundsException.class)
    public void check_transfer_too_much() throws ApplicationException {
        /// create ATM
        service.create("BANK_ATM", eurCurrencyCode, new BigDecimal("10000.00"));
        /// create Rick account
        service.create("Rick", eurCurrencyCode);

        service.transfer(service.get("BANK_ATM"), new BigDecimal("90000.00").negate(),
                service.get("Rick"), new BigDecimal("90000.00"),
                new BigDecimal("90000.00"), "EUR", UUID.randomUUID().toString());
    }

    @Test(expected = TransferAlreadyProcessedException.class)
    public void verify_resubmission_denied() throws ApplicationException {
        /// create ATM
        service.create("BANK_ATM", eurCurrencyCode, new BigDecimal("1000000.00"));
        /// create Rick account
        service.create("Rick", eurCurrencyCode);

        final String clientTransferId = UUID.randomUUID().toString();

        service.transfer(service.get("BANK_ATM"), new BigDecimal("90000.00"),
                service.get("Rick"), new BigDecimal("90000.00"),
                new BigDecimal("90000.00"), "EUR", clientTransferId);

        service.transfer(service.get("BANK_ATM"), new BigDecimal("90000.00"),
                service.get("Rick"), new BigDecimal("90000.00"),
                new BigDecimal("90000.00"), "EUR", clientTransferId);
    }

}
