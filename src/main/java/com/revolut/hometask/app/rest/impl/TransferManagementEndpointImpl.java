package com.revolut.hometask.app.rest.impl;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.revolut.hometask.app.business.*;
import com.revolut.hometask.app.errors.*;
import com.revolut.hometask.app.rest.TransferManagementEndpoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

@Singleton
public class TransferManagementEndpointImpl implements TransferManagementEndpoint {

    private final static Logger LOG = LoggerFactory.getLogger(TransferManagementEndpointImpl.class.getCanonicalName());

    private final AccountManagementService accountManagementService;

    private final ExchangeRateService exchangeRateService;

    private final MoneyTransferService moneyTransferService;

    private final ClockService clockService;

    @Inject
    public TransferManagementEndpointImpl(AccountManagementService accountManagementService,
                                          ExchangeRateService exchangeRateService,
                                          MoneyTransferService moneyTransferService, ClockService clockService) {
        this.accountManagementService = accountManagementService;
        this.exchangeRateService = exchangeRateService;
        this.moneyTransferService = moneyTransferService;
        this.clockService = clockService;
    }

    @Override
    public void transfer(String srcAccountId, String dstAccountId, String amountAsString, String currency,
                         String clientGeneratedTransferId)
            throws ApplicationDataAccessError, NoSufficientFundsException,
            TransferAlreadyProcessedException, AccountNotFoundException, AccountConcurrentModificationException {
        /// validate input parameters
        isCurrencyCodeValid(currency);
        ValidationUtils.isAccountIdValid(srcAccountId);
        ValidationUtils.isAccountIdValid(dstAccountId);

        if (clientGeneratedTransferId == null) {
            throw new IllegalArgumentException("Transfer client id is required.");
        }

        ///
        if (srcAccountId.equals(dstAccountId)) {
            throw new IllegalArgumentException("Cannot perform money self transfer.");
        }

        /// ensure amount value is correct
        BigDecimal amount = ValidationUtils.getAndValidateAmount(amountAsString);

        ///
        Account src = accountManagementService.get(srcAccountId);
        Account dst = accountManagementService.get(dstAccountId);

        ///
        long transactionTime = clockService.now();

        /// calls to rate service can be expensive, thus lets make a call in advance, and keep money in local variables
        // there is however chance that rate will float in between these two calls, i do not want to complicate
        // solution, yet we can make a bulk call to get proper rates
        LOG.info("Getting transfer amount converted. Amount={}{}", currency, amount);
        final BigDecimal fromAccountAmount = exchangeRateService.convertAmountBetweenCurrencies(currency, src.getCurrency(), amount, transactionTime);
        final BigDecimal toAccountAmount = exchangeRateService.convertAmountBetweenCurrencies(currency, dst.getCurrency(), amount, transactionTime);

        /// perform transfer logic
        LOG.info("Transferring from={} ({}-{}) to={} ({}{}) with id {}",
                srcAccountId, src.getCurrency(), fromAccountAmount,
                dstAccountId, dst.getCurrency(), toAccountAmount,
                clientGeneratedTransferId
        );
        moneyTransferService.transfer(src, fromAccountAmount.negate(), dst, toAccountAmount, amount, currency, clientGeneratedTransferId);
        LOG.info("Transferred from={} ({}-{}) to={} ({}{}) with id {}",
                srcAccountId, src.getCurrency(), fromAccountAmount,
                dstAccountId, dst.getCurrency(), toAccountAmount,
                clientGeneratedTransferId
        );
    }

    private void isCurrencyCodeValid(String currency) {
        if (!exchangeRateService.isCurrencyValid(currency)) {
            throw new IllegalArgumentException("Currency is not known " + currency);
        }
    }

}
