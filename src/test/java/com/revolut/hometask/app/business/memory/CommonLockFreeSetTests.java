package com.revolut.hometask.app.business.memory;

import com.revolut.hometask.app.business.ClockService;
import com.revolut.hometask.app.business.GenericTestMoneyTransfer;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mockito;

@RunWith(Parameterized.class)
public class CommonLockFreeSetTests extends GenericTestMoneyTransfer {

    public CommonLockFreeSetTests() {

        final InMemoryLockFreeStorage inMemoryLockFreeStorage = new InMemoryLockFreeStorage(Mockito.mock(ClockService.class));

        accountManagementService = inMemoryLockFreeStorage;
        transferService = inMemoryLockFreeStorage;
    }
}
