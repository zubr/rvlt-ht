package com.revolut.hometask.app.rest;

import com.revolut.hometask.app.errors.*;

public interface TransferManagementEndpoint {
    /**
     * Performs money transfer.
     *
     * @param srcAccountId              source account id
     * @param dstAccountId              destination account id
     * @param amount                    amount to transfer
     * @param currency                  currency of amount to transfer
     * @param clientGeneratedTransferId client generated unique transfer id
     */
    void transfer(String srcAccountId, String dstAccountId,
                  String amount, String currency,
                  String clientGeneratedTransferId)
            throws ApplicationDataAccessError, NoSufficientFundsException,
            TransferAlreadyProcessedException, AccountNotFoundException, AccountConcurrentModificationException;

}
