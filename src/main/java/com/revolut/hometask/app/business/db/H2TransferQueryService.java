package com.revolut.hometask.app.business.db;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.revolut.hometask.app.business.Transfer;
import com.revolut.hometask.app.business.TransferQueryService;
import com.revolut.hometask.app.errors.ApplicationDataAccessError;
import org.h2.jdbcx.JdbcConnectionPool;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Singleton
public class H2TransferQueryService implements TransferQueryService {

    private final JdbcConnectionPool connectionPool;

    private final String GET_TRANSFERS_BY_ACCOUNT = "select src_account_id, dst_account_id, currency, amount, created" +
            " from transfer where src_account_id = ? or dst_account_id = ?";

    private final String GET_TRANSFERS_TO_ACCOUNT = "select src_account_id, dst_account_id, currency, amount, created" +
            " from transfer where dst_account_id = ?";

    private final String GET_TRANSFERS_FROM_ACCOUNT = "select src_account_id, dst_account_id, currency, amount, created" +
            " from transfer where src_account_id = ?";

    @Inject
    public H2TransferQueryService(JdbcConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
    }

    @Override
    public List<Transfer> getTransfersForAccount(String account) throws ApplicationDataAccessError {
        try (final Connection connection = connectionPool.getConnection();
             final PreparedStatement ps = connection.prepareStatement(GET_TRANSFERS_BY_ACCOUNT)) {

            ps.setString(1, account);
            ps.setString(2, account);

            try (final ResultSet resultSet = ps.executeQuery()) {
                List<Transfer> transferList = new ArrayList<>();
                while (resultSet.next()) {
                    final Transfer transfer = parseTransferFromResultSet(resultSet);
                    transferList.add(transfer);
                }
                return transferList;
            }
        } catch (SQLException e) {
            throw new ApplicationDataAccessError(e);
        }
    }

    @Override
    public List<Transfer> getTransfersIntoAccount(String account) throws ApplicationDataAccessError {
        try (final Connection connection = connectionPool.getConnection();
             final PreparedStatement ps = connection.prepareStatement(GET_TRANSFERS_TO_ACCOUNT)) {

            ps.setString(1, account);

            try (final ResultSet resultSet = ps.executeQuery()) {
                List<Transfer> transferList = new ArrayList<>();
                while (resultSet.next()) {
                    final Transfer transfer = parseTransferFromResultSet(resultSet);
                    transferList.add(transfer);
                }
                return transferList;
            }
        } catch (SQLException e) {
            throw new ApplicationDataAccessError(e);
        }
    }

    @Override
    public List<Transfer> getTransfersPaymentsFromAccount(String account) throws ApplicationDataAccessError {
        try (final Connection connection = connectionPool.getConnection();
             final PreparedStatement ps = connection.prepareStatement(GET_TRANSFERS_FROM_ACCOUNT)) {

            ps.setString(1, account);

            try (final ResultSet resultSet = ps.executeQuery()) {
                List<Transfer> transferList = new ArrayList<>();
                while (resultSet.next()) {
                    final Transfer transfer = parseTransferFromResultSet(resultSet);
                    transferList.add(transfer);
                }
                return transferList;
            }
        } catch (SQLException e) {
            throw new ApplicationDataAccessError(e);
        }

    }

    private Transfer parseTransferFromResultSet(ResultSet resultSet) throws SQLException {
        final Transfer transfer = new Transfer();
        transfer.setAccountSourceId(resultSet.getString(1));
        transfer.setAccountDestinationId(resultSet.getString(2));
        transfer.setCurrency(resultSet.getString(3));
        transfer.setAmount(resultSet.getString(4));
        transfer.setWhen(resultSet.getTimestamp(5).getTime());
        return transfer;
    }
}
