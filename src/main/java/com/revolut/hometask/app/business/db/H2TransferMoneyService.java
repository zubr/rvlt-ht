package com.revolut.hometask.app.business.db;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.revolut.hometask.app.business.Account;
import com.revolut.hometask.app.business.ClockService;
import com.revolut.hometask.app.business.MoneyTransferService;
import com.revolut.hometask.app.errors.*;
import org.h2.jdbc.JdbcSQLIntegrityConstraintViolationException;
import org.h2.jdbcx.JdbcConnectionPool;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

@Singleton
public class H2TransferMoneyService implements MoneyTransferService {

    private final JdbcConnectionPool connectionPool;

    private final ClockService clockService;

    private final String UPDATE_ACCOUNT_AMOUNT_STMT = "update account set balance = ?, version = ? where account_id = ? and version = ?";

    private final String INSERT_TRANSACTION = "insert into transfer(src_account_id, dst_account_id, currency, amount, created, client_id) " +
            "values (?, ?, ?, ?, ?, ?)";

    @Inject
    public H2TransferMoneyService(JdbcConnectionPool connectionPool, ClockService clockService) {
        this.connectionPool = connectionPool;
        this.clockService = clockService;
    }

    @Override
    public void transfer(Account src, BigDecimal srcDiff, Account dst, BigDecimal dstDiff,
                         BigDecimal amount, String currency, String clientTransferId)
            throws ApplicationDataAccessError, TransferAlreadyProcessedException,
            NoSufficientFundsException, AccountConcurrentModificationException {
        /// get the time, and use it later to get conversion rate for specific time
        final long transactionTime = clockService.now();

        /// check we have enough money
        if (src.getBalance().compareTo(srcDiff.negate()) < 0) {
            throw new NoSufficientFundsException(src.getAccountId(), srcDiff.toString(), src.getCurrency());
        }

        doInternalMoneyTransfer(amount, currency, src, dst, srcDiff, dstDiff, transactionTime, clientTransferId);
    }

    private void doInternalMoneyTransfer(BigDecimal amount, String currency,
                                         Account src, Account dst,
                                         BigDecimal fromAccountAmount, BigDecimal toAccountAmount,
                                         long when, String clientTransferId)
            throws ApplicationDataAccessError, TransferAlreadyProcessedException, AccountConcurrentModificationException {
        final BigDecimal srcAccountNewBalance = src.getBalance().add(fromAccountAmount);
        final BigDecimal dstAccountNewBalance = dst.getBalance().add(toAccountAmount);

        try (final Connection connection = connectionPool.getConnection();
             final PreparedStatement psUpdateAccount = connection.prepareStatement(UPDATE_ACCOUNT_AMOUNT_STMT);
             final PreparedStatement psInsertTransfer = connection.prepareStatement(INSERT_TRANSACTION)) {

            connection.setAutoCommit(false);

            try {
                /// transfer money from source account
                updateAccountAmountConditionally(src, psUpdateAccount, srcAccountNewBalance);
                updateAccountAmountConditionally(dst, psUpdateAccount, dstAccountNewBalance);

                // safe to insert transaction
                try {
                    psInsertTransfer.setString(1, src.getAccountId());
                    psInsertTransfer.setString(2, dst.getAccountId());
                    psInsertTransfer.setString(3, currency);
                    psInsertTransfer.setString(4, amount.toString());
                    psInsertTransfer.setTimestamp(5, new Timestamp(when));
                    psInsertTransfer.setString(6, clientTransferId);

                    final int count = psInsertTransfer.executeUpdate();
                    if (count != 1) {
                        throw new ApplicationDataAccessError("Insert transaction failed.");
                    }
                } catch (JdbcSQLIntegrityConstraintViolationException e) {
                    connection.rollback();
                    throw new TransferAlreadyProcessedException(src.getAccountId(), clientTransferId);
                }

                // commit transaction
                connection.commit();
            } catch (SQLException | ApplicationDataAccessError | AccountConcurrentModificationException | TransferAlreadyProcessedException e) {
                connection.rollback();
                throw e;
            }
        } catch (SQLException e) {
            throw new ApplicationDataAccessError(e);
        }
    }

    private void updateAccountAmountConditionally(Account src,
                                                  PreparedStatement psUpdateAccount,
                                                  BigDecimal subtract)
            throws SQLException, AccountConcurrentModificationException {
        psUpdateAccount.setString(1, subtract.toString());
        psUpdateAccount.setLong(2, src.getVersion() + 1);
        psUpdateAccount.setString(3, src.getAccountId());
        psUpdateAccount.setLong(4, src.getVersion());
        final int count = psUpdateAccount.executeUpdate();
        if (count != 1) {
            throw new AccountConcurrentModificationException(src.getAccountId());
        }
    }
}
