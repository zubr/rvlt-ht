package com.revolut.hometask.app.rest;

import com.revolut.hometask.app.errors.AccountAlreadyExistsException;
import com.revolut.hometask.app.errors.AccountConcurrentModificationException;
import com.revolut.hometask.app.errors.AccountNotFoundException;
import com.revolut.hometask.app.errors.ApplicationDataAccessError;

/**
 * Account management endpoint actions.
 */
public interface AccountManagementEndpoint {

    void create(String accountId, String currency) throws AccountAlreadyExistsException, ApplicationDataAccessError;

    void update(String accountId, Long accountVersion, String amount) throws ApplicationDataAccessError,
            AccountConcurrentModificationException, AccountNotFoundException;

    AccountDescription get(String accountId) throws AccountNotFoundException, ApplicationDataAccessError;

}
