package com.revolut.hometask.app.business;

public interface ClockService {

    /**
     * @return returns current time
     */
    long now();

}
