package com.revolut.hometask.app.business;

import com.revolut.hometask.app.errors.ApplicationException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;


public abstract class GenericTestMoneyTransfer {

    protected static String accountIdSrc = "generic-transfer-src";

    protected static String accountIdDst = "generic-transfer-dst";

    protected static String currencyCodeEUR = "EUR";

    @org.junit.runners.Parameterized.Parameter(0)
    public String accSrcParameter;
    @org.junit.runners.Parameterized.Parameter(1)
    public String accSrcMoneyInit;
    @org.junit.runners.Parameterized.Parameter(2)
    public String accDstParameter;
    @org.junit.runners.Parameterized.Parameter(3)
    public String accDstMoneyInit;
    @org.junit.runners.Parameterized.Parameter(4)
    public TransferCase[] transferCases;
    protected MoneyTransferService transferService;
    protected AccountManagementService accountManagementService;

    @org.junit.runners.Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                /// normal money transfer
                {accountIdSrc + ".0", "1000.0", accountIdDst + ".0", "1.00",
                        new TransferCase[]{
                                new TransferCase("1.00", "999.00", "2.00"),
                                new TransferCase("1.00", "998.00", "3.00"),
                                new TransferCase("18.00", "980.00", "21.00"),
                        }},
                ///
                {accountIdSrc + ".1", "1000.0", accountIdDst + ".1", "0.00",
                        new TransferCase[]{
                                new TransferCase("1.00", "999.00", "1.00"),
                                new TransferCase("2.00", "997.00", "3.00"),
                                new TransferCase("3.00", "994.00", "6.00"),
                                new TransferCase("0.01", "993.99", "6.01"),
                        }},
        });
    }

    @Before
    public void prepare() throws ApplicationException {
        {
            accountManagementService.create(accSrcParameter, currencyCodeEUR);
            final Account account = accountManagementService.get(accSrcParameter);
            accountManagementService.update(accSrcParameter, account.getVersion(), new BigDecimal(accSrcMoneyInit));
        }
        {
            accountManagementService.create(accDstParameter, currencyCodeEUR);
            final Account account = accountManagementService.get(accDstParameter);
            accountManagementService.update(accDstParameter, account.getVersion(), new BigDecimal(accDstMoneyInit));
        }
    }

    @Test
    public void transfer_and_check_balance() throws ApplicationException {
        int txCounter = 0;
        for (TransferCase transferCase : transferCases) {
            final BigDecimal transferAmount = new BigDecimal(transferCase.amount);
            transferService.transfer(accountManagementService.get(accSrcParameter), transferAmount.negate(),
                    accountManagementService.get(accDstParameter), transferAmount,
                    transferAmount, currencyCodeEUR,
                    accSrcParameter + ".transfer." + txCounter);

            Account account = accountManagementService.get(accSrcParameter);
            Assert.assertEquals(new BigDecimal(transferCase.srcBalance), account.getBalance());

            account = accountManagementService.get(accDstParameter);
            Assert.assertEquals(new BigDecimal(transferCase.dstBalance), account.getBalance());

            txCounter++;
        }
    }

    private static class TransferCase {

        private final String amount;

        private final String srcBalance;

        private final String dstBalance;

        public TransferCase(String amount, String srcBalance, String dstBalance) {
            this.amount = amount;
            this.srcBalance = srcBalance;
            this.dstBalance = dstBalance;
        }
    }
}
