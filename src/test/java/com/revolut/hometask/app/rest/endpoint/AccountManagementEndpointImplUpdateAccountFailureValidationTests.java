package com.revolut.hometask.app.rest.endpoint;

import com.revolut.hometask.app.business.*;
import com.revolut.hometask.app.errors.AccountNotFoundException;
import com.revolut.hometask.app.errors.ApplicationDataAccessError;
import com.revolut.hometask.app.errors.ApplicationException;
import com.revolut.hometask.app.rest.impl.AccountManagementEndpointImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collection;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;


@RunWith(Parameterized.class)
public class AccountManagementEndpointImplUpdateAccountFailureValidationTests {

    private static final String srcAccountId = "junit-account";

    private static final long accountVersion = 77;

    @Parameterized.Parameter(0)
    public String accountIdParameter;
    @Parameterized.Parameter(1)
    public Long version;
    @Parameterized.Parameter(2)
    public String currency;

    private AccountManagementEndpointImpl service;
    private AccountManagementService accountManagementService;
    private ExchangeRateService exchangeRateService;
    private MoneyTransferService moneyTransferService;
    private ClockService clockService;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {srcAccountId, 1L, null},
                {srcAccountId, 1L, "AAA"},
                {srcAccountId + " ", 1L, "2312.0"},
                {srcAccountId, null, "2312.0"},
                {srcAccountId, 1L, "-167.0"},
                {srcAccountId, 1L, "0.0"}
        });
    }

    @Before
    public void prepare() throws ApplicationDataAccessError, AccountNotFoundException {
        accountManagementService = Mockito.mock(AccountManagementService.class);
        exchangeRateService = Mockito.mock(ExchangeRateService.class);
        moneyTransferService = Mockito.mock(MoneyTransferService.class);
        clockService = Mockito.mock(ClockService.class);

        final Account account = new Account();
        account.setVersion(accountVersion);
        when(accountManagementService.get(anyString()))
                .thenReturn(account);

        service = new AccountManagementEndpointImpl(accountManagementService, exchangeRateService);
    }

    @Test(expected = IllegalArgumentException.class)
    public void validate_account_creation_validation_logic() throws ApplicationException {
        service.update(accountIdParameter, version, currency);
    }

}
