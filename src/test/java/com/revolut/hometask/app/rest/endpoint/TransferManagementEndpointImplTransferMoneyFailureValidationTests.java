package com.revolut.hometask.app.rest.endpoint;

import com.revolut.hometask.app.business.AccountManagementService;
import com.revolut.hometask.app.business.ClockService;
import com.revolut.hometask.app.business.ExchangeRateService;
import com.revolut.hometask.app.business.MoneyTransferService;
import com.revolut.hometask.app.errors.ApplicationException;
import com.revolut.hometask.app.rest.impl.AccountManagementEndpointImpl;
import com.revolut.hometask.app.rest.impl.TransferManagementEndpointImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collection;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;


@RunWith(Parameterized.class)
public class TransferManagementEndpointImplTransferMoneyFailureValidationTests {

    private static final String srcAccountId = "junit-account-src";

    private static final String dstAccountId = "junit-account-dst";

    private static final String currencyCodeEUR = "EUR";

    private static final String currencyCodeInvalid = "Invalid";

    private TransferManagementEndpointImpl service;

    private AccountManagementService accountManagementService;

    private ExchangeRateService exchangeRateService;

    private MoneyTransferService moneyTransferService;

    private ClockService clockService;

    @Before
    public void prepare() {
        accountManagementService = Mockito.mock(AccountManagementService.class);
        exchangeRateService = Mockito.mock(ExchangeRateService.class);
        moneyTransferService = Mockito.mock(MoneyTransferService.class);
        clockService = Mockito.mock(ClockService.class);

        when(exchangeRateService.isCurrencyValid(eq(currencyCodeEUR))).thenReturn(true);
        when(exchangeRateService.isCurrencyValid(eq(currencyCodeInvalid))).thenReturn(false);

        service = new TransferManagementEndpointImpl(accountManagementService, exchangeRateService, moneyTransferService, clockService);
    }

    @org.junit.runners.Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {srcAccountId, srcAccountId, "1234.56", currencyCodeEUR},
                {srcAccountId, dstAccountId, "1234,56", currencyCodeEUR},
                {srcAccountId, dstAccountId, "1234.56", currencyCodeInvalid},
                {srcAccountId, dstAccountId, "-1234.56", currencyCodeEUR},
                {srcAccountId, dstAccountId, "0.0", currencyCodeEUR},
                {srcAccountId, dstAccountId, "A.0", currencyCodeEUR},
                {srcAccountId, dstAccountId, "(A).0", currencyCodeEUR},
        });
    }

    @org.junit.runners.Parameterized.Parameter(0)
    public String accSrcParameter;

    @org.junit.runners.Parameterized.Parameter(1)
    public String accDstParameter;

    @org.junit.runners.Parameterized.Parameter(2)
    public String amountParameter;

    @org.junit.runners.Parameterized.Parameter(3)
    public String currencyParameter;

    @Test(expected = IllegalArgumentException.class)
    public void validate_cannot_self_transfer() throws ApplicationException {
        when(exchangeRateService.isCurrencyValid(eq(currencyCodeEUR))).thenReturn(Boolean.TRUE);

        service.transfer(accSrcParameter, accDstParameter, amountParameter, currencyParameter, UUID.randomUUID().toString());
    }

}
