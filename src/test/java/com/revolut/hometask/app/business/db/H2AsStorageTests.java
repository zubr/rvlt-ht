package com.revolut.hometask.app.business.db;

import com.revolut.hometask.app.business.ClockService;
import com.revolut.hometask.app.business.GenericTestMoneyTransfer;
import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.sql.SQLException;

@RunWith(Parameterized.class)
public class H2AsStorageTests extends GenericTestMoneyTransfer {

    private static TestDatabaseConnectionProvider dbConnectionProvider;

    private static JdbcConnectionPool connectionPool;

    public H2AsStorageTests() {
        accountManagementService = new H2AccountService(connectionPool);
        transferService = new H2TransferMoneyService(connectionPool, new ClockService() {
            @Override
            public long now() {
                return System.currentTimeMillis();
            }
        });
    }

    @BeforeClass
    public static void prepareDb() {
        dbConnectionProvider = new TestDatabaseConnectionProvider();
        connectionPool = dbConnectionProvider.getConnectionPool();
    }

    @AfterClass
    public static void clearDb() throws SQLException {
        connectionPool.getConnection().prepareStatement("drop all objects").execute();
    }
}
