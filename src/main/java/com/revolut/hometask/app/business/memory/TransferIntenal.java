package com.revolut.hometask.app.business.memory;

import java.math.BigDecimal;

class TransferIntenal {

    private final BigDecimal amount;

    private final BigDecimal balance;

    private final String currency;

    private final AccountInternal src;

    private final AccountInternal dst;

    private final long when;

    private final String clientTransactionId;

    private final TransferIntenal next;

    protected TransferIntenal(BigDecimal amount, BigDecimal balance,
                              String currency, AccountInternal src, AccountInternal dst,
                              long when, String clientTransactionId, TransferIntenal next) {
        this.amount = amount;
        this.balance = balance;
        this.currency = currency;
        this.src = src;
        this.dst = dst;
        this.when = when;
        this.next = next;
        this.clientTransactionId = clientTransactionId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public AccountInternal getSrc() {
        return src;
    }

    public AccountInternal getDst() {
        return dst;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public String getClientTransactionId() {
        return clientTransactionId;
    }

    public TransferIntenal getNext() {
        return next;
    }

    public long getWhen() {
        return when;
    }

    @Override
    public boolean equals(Object o) {
        return false;
    }

    @Override
    public int hashCode() {
        int result = amount != null ? amount.hashCode() : 0;
        result = 31 * result + (currency != null ? currency.hashCode() : 0);
        result = 31 * result + (src != null ? src.hashCode() : 0);
        result = 31 * result + (dst != null ? dst.hashCode() : 0);
        return result;
    }
}
