package com.revolut.hometask.app.errors;

public class TransferAlreadyProcessedException extends ApplicationException {

    private final String srcAccountId;

    private final String clientTransferId;

    public TransferAlreadyProcessedException(String srcAccountId, String clientTransferId) {
        this.srcAccountId = srcAccountId;
        this.clientTransferId = clientTransferId;
    }

    public String getSrcAccountId() {
        return srcAccountId;
    }

    public String getClientTransferId() {
        return clientTransferId;
    }
}
